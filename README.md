# **NeBotik** #


NeBotik is configurable and very well customizable personal chat bot. It provides API for processing and replying to a messages on Facebook (or just mark incoming messages as seen).
* Emulates common chrome browser to pull data from Facebook webpages (polling mechanism).
* Somehow also supports Skype and Ask.fm
* Even though all libraries are provided, maven build is recommended
* UI designed to fit Raspberry Pi 3.5 display (resolution 480×320) 

To get the application running, you are required to create few additional files:

* FacebookSettings class containing valid login credentials and your Facebook name (The one that appear on every page after login).
* AskSettings class. Depending on captcha mechanism, either provide valid cookie file or valid credentials (or just null values to turn Ask.fm support off)
* SkypeSettings class with valid login credentials (or just null values to turn Skype support off) 
* Whatever Eclipse suggests you to create
* maven goal "clean compile assembly:single" to get standalone jar file

** Skype doesn't work properly **
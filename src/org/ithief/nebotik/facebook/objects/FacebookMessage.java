package org.ithief.nebotik.facebook.objects;

import java.util.List;
import java.util.regex.Pattern;

import org.ithief.nebotik.objects.Message;

import com.gargoylesoftware.htmlunit.html.DomAttr;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.DomText;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;

public class FacebookMessage implements Message {

	private String sender;
	private String content;
	private String send_date;
	private boolean is_online;
	private boolean is_read = true;
	private int unreadCount = 0;

	private String readLink;
	private String onlineIcon = "";

	private boolean valid = true;

	public static final int HOURS = 1;
	public static final int MINUTES = 2;
	public static final int WEEK = 3;
	public static final int DAYS = 4;

	private int dateClassifier;
	private FacebookRawClient client;

	private void classifyDate() {
		if (send_date.endsWith("h")) {
			dateClassifier = HOURS;
		} else if (send_date.endsWith("min") || send_date.equals("Pr�v� te�")) {
			dateClassifier = MINUTES;
		} else {
			String a = send_date.substring(0, 1);
			try { // First letter is number -> days
				Integer.parseInt(a);
				dateClassifier = DAYS;
			} catch (Exception e) {
				dateClassifier = WEEK;
			}
		}
	}

	public boolean hasBeenMinutes() {
		return dateClassifier == MINUTES;
	}

	public boolean hasBeenHours() {
		return dateClassifier == HOURS;
	}

	public boolean hasBeenWeek() {
		return dateClassifier == WEEK;
	}

	public boolean hasBeenDays() {
		return dateClassifier == DAYS;
	}

	public FacebookRawClient getClient() {
		return client;
	}

	public FacebookMessage(FacebookRawClient facebookClient, DomNode node) {
		this.client = facebookClient;
		try {
			readLink = ((DomAttr) node.getByXPath("tbody/tr/td/div/h3/a/@href").get(0)).getValue();
			sender = node.getByXPath("tbody/tr/td/div/h3/a/text()").get(0).toString().trim();
			if (sender.endsWith(")")) {
				String[] a = sender.split(Pattern.quote("("));
				sender = a[0].trim();
				String count = a[1].substring(0, a[1].length() - 1);
				try {
					unreadCount = Integer.parseInt(count);
				} catch (Exception e) {

				}
			}

			List<?> s = node.getByXPath("tbody/tr/td/div/h3/a/img/@src");
			if (s.size() == 1) {
				is_online = true;
				onlineIcon = ((DomAttr) s.get(0)).getValue();
			} else {
				is_online = false;
			}

			send_date = node.getByXPath("tbody/tr/td/div/h3/span/abbr/text()").get(0).toString();
			content = "";
			List<?> obj = node.getByXPath("(tbody/tr/td/div/h3)[2]/span");
			if (obj.size() == 0) {
				obj = node.getByXPath("(tbody/tr/td/div/h3)[2]/strong/span");
				is_read = false;
				unreadCount = 1;
			}

			if (obj.size() > 0) {
				HtmlSpan a = (HtmlSpan) obj.get(0);
				StringBuilder sb = new StringBuilder();
				DomNodeList<DomNode> chldrn = a.getChildNodes();
				for (DomNode nd : chldrn) {
					if (nd instanceof DomText) {
						sb.append(nd.asText() + " ");
					} else {
						nd.getAttributes().removeNamedItem("class");
						sb.append(nd.asXml());
					}
				}
				content = sb.toString().trim();
			} else {
				content = "";
			}
			classifyDate();
		} catch (Exception e) {
			valid = false;
		}
	}

	@Override
	public boolean isValid() {
		return valid;
	}

	@Override
	public String getContents() {
		return content;
	}

	public int getUnreadCount() {
		return unreadCount;
	}

	public String getOnlineIcon() {
		return onlineIcon;
	}

	public String getReadLink() {
		return readLink;
	}

	@Override
	public String getSender() {
		return sender;
	}

	public String getSendDate() {
		return send_date;
	}

	public boolean isOnline() {
		return is_online;
	}

	public boolean isRead() {
		return is_read;
	}

	public String getAsString() {
		return "[" + sender + "]: " + content;
	}

	private boolean old = false;

	/**
	 * If message has been received before, but no reaction was performed
	 * 
	 * @return
	 */
	public boolean isOld() {
		return old;
	}

	public void setOld(boolean b) {
		this.old = b;
	}

	@Override
	public String toString() {
		return sender + ": " + content;
	}

	@Override
	public String getID() {
		return toString();
	}
}

package org.ithief.nebotik.facebook.objects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;

public class PostsPage {

	private String linkToNextPage;
	private List<Post> posts = new ArrayList<>();

	private boolean valid = true;
	private FacebookRawClient client;

	public boolean hasNextPage() {
		return linkToNextPage != null;
	}

	public FacebookRawClient getClient() {
		return client;
	}

	public PostsPage(FacebookRawClient facebookClient, DomNode node) {
		this.client = facebookClient;
		try {
			List<?> pg = node.getByXPath("//*[@id='tlFeed']");
			DomNode n = (DomNode) pg.get(0);
			n = n.getFirstChild().getFirstChild().getFirstChild().getFirstChild();
			Iterator<DomNode> it = n.getChildren().iterator();
			while (it.hasNext()) {
				DomNode ob = it.next();
				Post post = new Post(facebookClient, ob);
				if (post.isValid()) {
					posts.add(post);
				}
			}
			DomNode ng = (DomNode) node.getByXPath("//*[@id='structured_composer_async_container']").get(0);
			DomNode ag = (DomNode) ng.getChildNodes().get(1).getFirstChild();
			linkToNextPage = null;
			if (ag.asText().equals("Zobrazit dal��")) {
				if (ag instanceof HtmlAnchor) {
					linkToNextPage = ((HtmlAnchor) ag).getHrefAttribute();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			valid = false;
		}
	}

	public boolean isValid() {
		return valid;
	}

	public String getLinkToNextPage() {
		return linkToNextPage;
	}

	public List<Post> getPosts() {
		return posts;
	}

}

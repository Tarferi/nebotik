package org.ithief.nebotik.facebook.objects;

import org.ithief.nebotik.ui.ServiceStatus;
import org.ithief.nebotik.ui.ServiceUI;

public class OfflineHandler {

	private Thread thr = new Thread(new Runnable() {

		@Override
		public void run() {
			handle();
		}
	});
	private FacebookRawClient hc;
	private ServiceUI uc;

	private final Object syncer = new Object();

	private boolean loop = true;

	private void handle() {
		synchronized (syncer) {
			while (loop) {
				if (loop) {
					process();
				}
				try {
					syncer.wait(10 * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (!loop) {
					return;
				}
			}
		}
	}

	private void process() {
		long lr = hc.getLastOKRequest();
		long cr = System.currentTimeMillis() / 1000;
		long crlr = cr - lr;
		if (crlr < 55) { // 55 seconds between last OK request and now, still online
			if (!uc.getStatus().equals(ServiceStatus.Waiting)) { // If not waiting -> first valid online run
				uc.setStatus(ServiceStatus.Online);
			}
		} else {
			uc.setStatus(ServiceStatus.Offline);
		}
	}

	public void asyncNotify() {
		process();
	}

	public OfflineHandler(FacebookRawClient c, ServiceUI ui) {
		this.hc = c;
		this.uc = ui;
		thr.start();
	}

	public void stop() {
		uc.setStatus(ServiceStatus.Stopping);
		loop = false;
		synchronized (syncer) {
			syncer.notifyAll();
		}
		uc.setStatus(ServiceStatus.Offline);
	}

}

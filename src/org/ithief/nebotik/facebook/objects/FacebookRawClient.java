package org.ithief.nebotik.facebook.objects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ithief.nebotik.facebook.FacebookSettings;
import org.ithief.nebotik.raw.ErrorClient;
import org.ithief.nebotik.raw.HTTPClient;
import org.ithief.nebotik.ui.ServiceUI;

import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlRadioButtonInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class FacebookRawClient extends ErrorClient {

	private boolean messagesUpdated = false;

	private final OfflineHandler oh;

	private List<FacebookMessage> lastMessages = new ArrayList<>();

	private final HTTPClient hc = new HTTPClient();

	private boolean isStillLogged(boolean check) {
		try {
			if (hc.getLastPage() == null) {
				check = true;
			}
			debugInfo("Checking if previous login is still available");
			if (check) {
				hc.updatePage("https://m.facebook.com/");
			}
			boolean res = hc.getLastPage().asXml().contains(FacebookSettings.myLoggedString);
			if (res) {
				debugInfo("Login IS available from previous session");
				updateRequest();
			} else {
				debugInfo("Login is NOT available");
			}
			return res;
		} catch (Exception e) {
			setError(e);
			return false;
		}
	}

	private void updateRequest() {
		lastOkRequest = hc.getLastRequest();
	}

	private long lastOkRequest = 0;

	public long getLastOKRequest() {
		return lastOkRequest;
	}

	private void debugInfo(String string) {

	}

	public FacebookRawClient(ServiceUI ui) {
		oh = new OfflineHandler(this, ui);
	}

	@Override
	public boolean hasError() {
		boolean hce = hc.hasError();
		if (hce) { // If raw client has error, we have error
			return true;
		} else {
			return super.hasError();
		}
	}

	@Override
	public List<Exception> getExceptions() {
		List<Exception> list = new ArrayList<>();
		list.addAll(super.getExceptions());
		list.addAll(hc.getExceptions());
		return list;
	}

	@Override
	public void clearError() {
		hc.clearError();
		super.clearError();
	}

	public boolean deletePost(Post post) {
		try {
			if (post.hasOptionsLink()) {
				hc.updatePage("https://m.facebook.com" + post.getLinkToOptions());
				List<DomElement> els = hc.getLastPage().getElementsByName("action_key");
				for (DomElement el : els) {
					if (el instanceof HtmlRadioButtonInput) {
						HtmlRadioButtonInput e = (HtmlRadioButtonInput) el;
						if (el.getAttribute("value").equals("DELETE")) {
							e.setChecked(true);
							List<DomElement> btns = hc.getLastPage().getElementsByTagName("input");
							for (DomElement btn : btns) {
								if (btn instanceof HtmlSubmitInput) {
									if (btn.getAttribute("value").equals("Pokračovat")) {
										updateRequest();
										hc.updatePage(btn); // TODO: Verify
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			setError(e);
		}
		return false;
	}

	public List<FacebookMessage> getMessages(boolean update) {
		try {
			if (update || !messagesUpdated) {
				messagesUpdated = true;
				lastMessages.clear();
				hc.updatePage("https://m.facebook.com/messages/?ref_component=mbasic_home_header");
				if (!isStillLogged(false)) {
					if (!login()) {
						return null;
					}
				}
				Iterator<DomNode> it = hc.getLastPage().getElementById("root").getFirstElementChild().getChildren().iterator();
				it.next();
				Iterable<DomNode> mgs = it.next().getChildren().iterator().next().getChildren();
				for (DomNode node : mgs) {
					FacebookMessage msg = new FacebookMessage(this, node);
					if (msg.isValid()) {
						lastMessages.add(msg);
					}
				}
			}
		} catch (Exception e) {
			setError(e);
		}
		return lastMessages;
	}

	public void init() {
		hc.init();
	}

	public void clearMessagesUpdated() {
		messagesUpdated = false;
	}

	public boolean login() {
		try {
			debugInfo("Logging in");
			if (isStillLogged(false)) {
				return true;
			}
			if (!isStillLogged(true)) {
				debugInfo("Creating new login");
				hc.updatePage("https://m.facebook.com/");
				HtmlTextInput email = (HtmlTextInput) hc.getLastPage().getElementsByName("email").get(0);
				HtmlPasswordInput pass = (HtmlPasswordInput) hc.getLastPage().getElementsByName("pass").get(0);
				DomElement lb = hc.getLastPage().getElementByName("login");

				email.setValueAttribute(FacebookSettings.email);
				pass.setValueAttribute(FacebookSettings.password);
				if (lb instanceof HtmlButton) {
					HtmlButton loginBtn = (HtmlButton) lb;
					hc.updatePage(loginBtn);
				} else if (lb instanceof HtmlSubmitInput) {
					HtmlSubmitInput loginBtn = (HtmlSubmitInput) lb;
					hc.updatePage(loginBtn);
				} else {
					System.err.println("Unknown button class: " + lb);
				}
			}
			debugInfo("Logged in");
			return isStillLogged(false);
		} catch (Exception e) {
			setError(e);
			return false;
		}
	}

	public PostsPage getNextPage(PostsPage page) {
		try {
			if (page.hasNextPage()) {
				if (!isStillLogged(false)) { // Not logged so far
					login();
					if (!isStillLogged(false)) { // Login expiration?
						return null;
					}
				}
				hc.updatePage("https://m.facebook.com" + page.getLinkToNextPage());
				DomElement pg = hc.getLastPage().getElementById("tlFeed");
				if (pg != null) {
					return new PostsPage(page.getClient(), pg);
				}
			}
		} catch (Exception e) {
			setError(e);
		}
		return null;
	}

	public void readMessage(FacebookMessage msg) {
		try {
			System.out.println("Reading message from " + msg.getSender());
			hc.updatePage("https://m.facebook.com" + msg.getReadLink());
		} catch (Exception e) {
			setError(e);
		}
	}

	public void replyTo(FacebookMessage reply, String response) {
		try {
			hc.updatePage("https://m.facebook.com" + reply.getReadLink());
			HtmlTextArea area = (HtmlTextArea) hc.getLastPage().getElementById("composerInput");
			HtmlSubmitInput sendBtn = (HtmlSubmitInput) area.getParentNode().getParentNode().getChildNodes().get(1).getChildNodes().get(0);
			area.setText(response);
			hc.updatePage(sendBtn);
			System.out.println("Replying to " + reply.getSender());
		} catch (Exception e) {
			setError(e);
		}
	}

	public PostsPage getFirstPage() {
		try {
			if (!isStillLogged(false)) { // Not logged so far
				login();
				if (!isStillLogged(false)) { // Login expiration?
					return null;
				}
			}
			hc.updatePage("https://m.facebook.com/venomio?ref_component=mbasic_home_header&ref_page=%2Fwap%2Fhome.php&refid=8");
			DomElement pg = hc.getLastPage().getElementById("tlFeed");
			if (pg != null) {
				return new PostsPage(this, pg);
			}
		} catch (Exception e) {
			setError(e);
		}
		return null;
	}

	public void close() {
		oh.stop();
		hc.close();
	}

}

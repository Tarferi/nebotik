package org.ithief.nebotik.facebook.objects;

import java.util.List;

import org.w3c.dom.Node;

import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;

public class Post {

	private String title;
	private String content;

	private String agoString;
	private String privacy;

	private String linkToPrivacy;
	private String linkToOptions;

	private boolean valid = true;
	private FacebookRawClient client;

	public boolean hasPrivacyLink() {
		return linkToPrivacy != null;
	}

	public FacebookRawClient getClient() {
		return client;
	}

	public Post(FacebookRawClient facebookClient, DomNode node) {
		this.client = facebookClient;
		try {
			title = node.getChildNodes().get(0).getChildNodes().get(0).asText().trim();
			DomNodeList<DomNode> nng = node.getChildNodes().get(0).getChildNodes();
			if (nng.size() > 1) {
				content = nng.get(1).asText();
			} else {
				content = "";
			}
			DomNode ng = node.getChildNodes().get(0).getChildNodes().get(2);
			if (ng != null) {
				content += "\n" + ng.asText();
			}
			ng = node.getChildNodes().get(1).getChildNodes().get(0);
			agoString = ng.getFirstChild().asText();
			nng = ng.getChildNodes();
			ng = nng.get(nng.size() - 1);
			nng = ng.getFirstChild().getChildNodes();
			List<?> hg = ng.getByXPath("*/span");
			ng = (DomNode) hg.get(hg.size() - 1);
			privacy = ng.asText();
			Node href = ng.getFirstChild().getAttributes().getNamedItem("href");
			if (href != null) {
				linkToPrivacy = href.getNodeValue();
			} else {
				linkToPrivacy = null;
			}
			nng = node.getChildNodes().get(1).getChildNodes().get(1).getChildNodes();
			ng = nng.get(nng.size() - 1);
			linkToOptions = ng.getAttributes().getNamedItem("href").getNodeValue();
		} catch (Exception e) {
			e.printStackTrace();
			valid = false;
		}
	}

	public String getAgo() {
		return agoString;
	}

	public String getPricacy() {
		return privacy;
	}

	public boolean isValid() {
		return valid;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public String getLinkToPrivacy() {
		return linkToPrivacy;
	}

	public String getLinkToOptions() {
		return linkToOptions;
	}

	public boolean hasOptionsLink() {
		return linkToOptions != null;
	}
}

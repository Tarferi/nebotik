package org.ithief.nebotik.facebook;

import org.ithief.nebotik.database.Database;
import org.ithief.nebotik.facebook.objects.FacebookRawClient;
import org.ithief.nebotik.facebook.objects.Post;
import org.ithief.nebotik.facebook.objects.PostsPage;
import org.ithief.nebotik.objects.behavior.Behavior;
import org.ithief.nebotik.objects.behavior.custom.MessageReplier;
import org.ithief.nebotik.ui.MessageServiceUI;
import org.ithief.nebotik.ui.ServiceStatus;
import org.ithief.nebotik.ui.ServiceUI;

public class FacebookClient extends BehavioralClient {

	private final FacebookRawClient hc;

	private Thread thread = new Thread() {
		@Override
		public void run() {
			uis.setStatus(ServiceStatus.Starting);
			init();
			loop();
			uis.setStatus(ServiceStatus.Stopping);
			close();
			uis.setStatus(ServiceStatus.Offline);
		}
	};

	private static final int ErrorRerunDelay = 60 * 60; // 60 Minutes after 10

	private static final int RoutineDelay = 45; // 30 Seconds between routines
	private int bad_runs = 0;

	private MessageServiceUI uis;

	public FacebookClient(MessageServiceUI ui) {
		this.uis = ui;
		hc = new FacebookRawClient(ui);
		run();
	}

	public Database getDatabase() {
		return uis.getDB();
	}

	public FacebookRawClient getFacebookClient() {
		return hc;
	}

	@Override
	public boolean safeBehave(ServiceUI ui, Behavior b) {
		if (ui instanceof MessageServiceUI) {
			try {
				if (!b.behave(ui, ((MessageServiceUI) ui).behaviorIsEnabled(), true)) {
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			return !hc.hasError();
		} else {
			return false;
		}
	}

	private final void routine() {
		try {
			hc.clearError(); // Clear all previous errors
			bad_runs++; // Increment bad runs
			if (hc.login()) { // Login is prerequisite
				if (!hc.hasError()) {
					hc.clearMessagesUpdated(); // Next call to getMessages will update the messages
					if (!runBehaviors(uis)) { // Behavioral failure
						return;
					}
					if (!hc.hasError()) { // All went well
						bad_runs = 0; // Clear error counter
						uis.getDB().commit(); // Save database
						return;
					}
				}
			} else { // Failed to login -> this is error
				System.err.println("Routine error (Failed to login)");
				return; // Stop all actions
			}
		} catch (Exception e) {
			e.printStackTrace();
			bad_runs++;
		}
	}

	private final Object syncer = new Object();

	private void mWait(int delayInSeconds) {
		try {
			uis.setStatus(ServiceStatus.Waiting);
			synchronized (syncer) {
				syncer.wait(delayInSeconds * 1000);
			}
			uis.setStatus(ServiceStatus.Online);
		} catch (Exception e) {
			bad_runs++;
			e.getSuppressed();
		}
	}

	private boolean doLoop = true;

	protected void loop() {
		uis.handleStatusChanged("Entering loop");
		while (doLoop) {
			System.out.print(".");
			uis.handleStatusChanged("Entering routines");
			routine();
			uis.handleStatusChanged("Routines finished");
			System.out.print(",");
			uis.handleNewError(bad_runs);
			if (hc.hasError()) {
				uis.handleExceptions(hc.getExceptions());
			}
			if (bad_runs != 0) {
				System.err.println("Total errors: " + bad_runs);
			}
			if (bad_runs > 10) { // 10 Bad runs will end the program
				uis.handleStatusChanged("More than 10 errors, stopping execution");
				return;
			} else if (bad_runs > 4) { // 4 Bad runs will cause 1 hour of inactivity
				uis.handleStatusChanged("Waiting for " + ErrorRerunDelay + " seconds, errors >4");
				mWait(ErrorRerunDelay);
			} else if (bad_runs != 0) { // First 4 bad runs cause 10 minutes delay
				uis.handleStatusChanged("Waiting for " + 600 + " seconds, errors 0 < ec <= 4");
				mWait(10 * 60);
			} else { // Regular delay
				uis.handleStatusChanged("Waiting for " + RoutineDelay + " seconds, no errors");
				mWait(RoutineDelay);
			}
			if (bad_runs > 0) {
				System.out.println("\nBad runs: " + bad_runs);
			}
		}
	}

	protected void iteratePosts() {
		PostsPage page = hc.getFirstPage();
		if (page != null) {
			while (page.hasNextPage()) {
				for (Post post : page.getPosts()) {
					System.out.println("=======================================");
					System.out.println(post.getTitle() + " (" + post.getAgo() + ")");
					System.out.println(post.getContent());
					System.out.println("=======================================\n");
					if (post.getContent().equals("TEST")) {
						hc.deletePost(post);
						return;
					}
				}
				page = hc.getNextPage(page);
				if (page == null) {
					break;
				}
			}
		}
	}

	private void init() {
		hc.init();
		registerBehavior(new MessageReplier(uis));
	}

	private void uninit() {
		hc.close();
	}

	private void run() {
		thread.start();
	}

	public void close() {
		doLoop = false;
		forceUpdate();
		uninit();
	}

	public void forceUpdate() {
		try {
			synchronized (syncer) {
				syncer.notifyAll();
			}
		} catch (Exception e) {
		}
	}
}

package org.ithief.nebotik.facebook;

import java.util.ArrayList;
import java.util.List;

import org.ithief.nebotik.objects.behavior.Behavior;
import org.ithief.nebotik.ui.ServiceUI;

public abstract class BehavioralClient {

	private List<Behavior> behaviors = new ArrayList<>();

	protected abstract boolean safeBehave(ServiceUI ui, Behavior b);

	public void registerBehavior(Behavior b) {
		if (!this.behaviors.contains(b)) {
			behaviors.add(b);
		}
	}

	public void unregisterBehavior(Behavior b) {
		if (this.behaviors.contains(b)) {
			behaviors.remove(b);
		}
	}

	public boolean runBehaviors(ServiceUI ui) {
		for (Behavior b : behaviors) {
			if (!safeBehave(ui, b)) { // Behavior caused error
				System.err.println("Routine error (Behavior " + b.getClass() + " failed)");
				return false; // Stop all actions
			}
		}
		return true;
	}

}

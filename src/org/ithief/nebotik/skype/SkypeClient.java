package org.ithief.nebotik.skype;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import org.ithief.nebotik.objects.Message;
import org.ithief.nebotik.objects.behavior.custom.MessageReplier;
import org.ithief.nebotik.raw.ErrorClient;
import org.ithief.nebotik.ui.MessageServiceUI;
import org.ithief.nebotik.ui.ServiceStatus;
import org.ithief.nebotik.ui.skype.SkypePanel;

import com.samczsun.skype4j.Skype;
import com.samczsun.skype4j.SkypeBuilder;
import com.samczsun.skype4j.Visibility;
import com.samczsun.skype4j.chat.Chat;
import com.samczsun.skype4j.chat.messages.ReceivedMessage;
import com.samczsun.skype4j.events.EventHandler;
import com.samczsun.skype4j.events.Listener;
import com.samczsun.skype4j.events.chat.message.MessageReceivedEvent;
import com.samczsun.skype4j.events.contact.ContactRequestEvent;
import com.samczsun.skype4j.exceptions.ConnectionException;
import com.samczsun.skype4j.user.Contact;

public class SkypeClient extends ErrorClient {

	private final MessageReplier beh;

	private Thread thr = new Thread() {
		@Override
		public void run() {
			handle();
		}
	};

	private MessageServiceUI ui;
	private Skype skype;

	public SkypeClient(MessageServiceUI ui) {
		this.ui = ui;
		this.beh = new MessageReplier(ui);
		thr.start();
	}

	private void handle() {
		this.clearError();
		ui.setStatus(ServiceStatus.Starting);
		ui.setTextOnMainScreen("S", Color.cyan);
		try {
			skype = new SkypeBuilder(SkypeSettings.username, SkypeSettings.password).withAllResources().build();
			skype.login();

			skype.getEventDispatcher().registerListener(new Listener() {

				@EventHandler
				public void onMessage(MessageReceivedEvent e) {
					SkypeClient.this.onMessage(e);
				}

				@EventHandler
				public void onContactRequest(ContactRequestEvent e) {
					SkypeClient.this.onContact(e);
				}

			});
			skype.subscribe();
			skype.loadAllContacts();
			skype.setVisibility(Visibility.ONLINE);
			ui.hideTextOnMainScreen();
			ui.setStatus(ServiceStatus.Online);
		} catch (Exception e) {
			e.printStackTrace();
			this.setError(e);
			ui.handleError("Failed to log in");
			ui.setTextOnMainScreen("S", Color.red);
			close();
		}
	}

	private Map<String, String> lastMessages = new HashMap<>();

	private boolean hasBeenReceivedBefore(ReceivedMessage msg) {
		String lastId = lastMessages.get(msg.getSender().getUsername());
		if (lastId != null) {
			return lastId.equals(msg.getId());
		}
		return false;
	}

	private void addToReceivedMessages(ReceivedMessage msg) {
		lastMessages.put(msg.getSender().getUsername(), msg.getId());
	}

	private void onMessage(MessageReceivedEvent event) {
		try {
			if (!hasBeenReceivedBefore(event.getMessage())) { // New message to us
				ui.setTextOnMainScreen("S", Color.yellow);
				addToReceivedMessages(event.getMessage());
				String username = event.getMessage().getSender().getUsername();
				String fname = event.getMessage().getSender().getContact().getFirstName();
				String lname = event.getMessage().getSender().getContact().getLastName();
				String dname = event.getMessage().getSender().getDisplayName();
				String name;
				if (fname != null && lname != null) {
					name = fname + " " + lname;
				} else if (dname != null) {
					name = dname;
				} else {
					name = username;
				}
				String content = event.getMessage().getContent().asPlaintext();
				Message msg = new SkypeMessage(name, content, event.getMessage().getSentTime(), event.getChat(), event.getMessage().getId());
				ui.processMessage(msg);
				ui.handleMessage(name + ": " + content);
				if (ui.behaviorIsEnabled()) {
					beh.behave(ui, ui.behaviorIsEnabled(), false);
				}
			}
		} catch (Exception e) {
			setError(e);
			ui.handleExceptions(this.getExceptions());
		}
	}

	/**
	 * Still not working properly
	 * 
	 * @param event
	 */
	public void onContact(ContactRequestEvent event) {
		try {
			SkypePanel pnl = (SkypePanel) ui;
			if (pnl.isAcceptingRequests()) {
				Contact user = event.getRequest().getSender();
				ui.handleMessage(user.getUsername() + " added me.");
				event.getRequest().accept();
				user.getPrivateConversation().sendMessage("Ahoj :)");
			}
		} catch (Exception e) {
			this.setError(e);
			ui.handleExceptions(this.getExceptions());
		}
	}

	public void close() {
		ui.setStatus(ServiceStatus.Offline);
		ui.hideTextOnMainScreen();
		try {
			skype.logout();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	public void handleException(Exception e) {
		this.setError(e);
		ui.handleError("Failed to log in");
		ui.setTextOnMainScreen("S", Color.red);
		close();
	}

	public void replyTo(Message msg, String response) {
		this.clearError();
		if (msg instanceof SkypeMessage) {
			SkypeMessage m = (SkypeMessage) msg;
			Chat chat = m.getChat();
			try {
				chat.sendMessage(response);
			} catch (ConnectionException e) {
				this.setError(e);
				ui.handleExceptions(this.getExceptions());
				e.printStackTrace();
			}
		}
	}
}

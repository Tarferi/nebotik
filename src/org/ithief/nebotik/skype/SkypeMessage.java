package org.ithief.nebotik.skype;

import org.ithief.nebotik.objects.Message;

import com.samczsun.skype4j.chat.Chat;

public class SkypeMessage implements Message {

	private String sender;
	private String contents;
	private long time;
	private Chat chat;
	private String id;

	public SkypeMessage(String sender, String contents, long time, Chat chat, String id) {
		this.sender = sender;
		this.contents = contents;
		this.time = time;
		this.chat = chat;
		this.id = id;
	}

	@Override
	public String getSender() {
		return sender;
	}

	@Override
	public String getContents() {
		return contents;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public String getAsString() {
		return sender + ": " + contents;
	}

	private boolean old = false;

	@Override
	public void setOld(boolean b) {
		old = b;
	}

	private boolean read = false;

	@Override
	public boolean isRead() {
		return read;
	}

	@Override
	public boolean isOld() {
		return old;
	}

	@Override
	public int getUnreadCount() {
		return 1;
	}

	private final int getDiff() {
		return (int) ((System.currentTimeMillis() / 1000) - time);
	}

	@Override
	public boolean hasBeenMinutes() {
		return getDiff() < 60 * 60;
	}

	@Override
	public boolean hasBeenHours() {
		return getDiff() < 60 * 60 * 24;
	}

	@Override
	public boolean hasBeenWeek() {
		return getDiff() < 60 * 60 * 24 * 7;
	}

	@Override
	public boolean hasBeenDays() {
		return getDiff() > 60 * 60 * 24 * 7;
	}

	public Chat getChat() {
		return chat;
	}

	@Override
	public String getID() {
		return id;
	}

	@Override
	public String toString() {
		return getAsString();
	}

	@Override
	public boolean equals(Object msg) {
		if (msg instanceof SkypeMessage) {
			return id.equals(((SkypeMessage) msg).getID());
		} else {
			return super.equals(msg);
		}
	}

}

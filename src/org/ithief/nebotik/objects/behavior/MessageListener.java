package org.ithief.nebotik.objects.behavior;

import java.util.List;

import org.ithief.nebotik.objects.Message;

public interface MessageListener {

	public void getResponseForMessage(List<String> responses, Message message);

}

package org.ithief.nebotik.objects.behavior;

import java.util.List;

import org.ithief.nebotik.objects.Message;

public interface CommandListener {

	public void getResponseForCommand(List<String> result, Message messagee, String command, String... params);

}

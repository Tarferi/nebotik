package org.ithief.nebotik.objects.behavior.custom;

import java.util.List;

import org.ithief.nebotik.database.Database;
import org.ithief.nebotik.database.SenderDatabase;
import org.ithief.nebotik.objects.Message;
import org.ithief.nebotik.objects.behavior.MessageListener;
import org.ithief.nebotik.ui.MessageServiceUI;

public class BasicMessages implements MessageListener {

	private SenderDatabase sent1;
	private SenderDatabase sent2;
	private SenderDatabase sent3;

	private static final String notHere = "Ahoj, Tom� tady te� nen�. D�m mu v�d�t jakmile se vr�t�.";
	private static final String notMe = "Nev�m komu v�emu o m� co Tom� napov�dal, ale asi se nezn�me :). A� se vr�t�, tak se ho m��eme zeptat :D.";
	private static final String notAsleep = "To z�le�� na tom, koho mysl� :D. Tom� sp� u� hodn� dlouho... a j� za chv�li p�jdu tak� :)";

	public BasicMessages(MessageServiceUI c) {
		Database db = c.getDB();
		sent1 = new SenderDatabase(db, "MessageReplier_1");
		sent2 = new SenderDatabase(db, "MessageReplier_2");
		sent3 = new SenderDatabase(db, "MessageReplier_3");
	}

	@Override
	public void getResponseForMessage(List<String> responses, Message message) {
		String question = MessageReplier.stripAccents(message.getContents().toLowerCase());
		if (question.contains("jsi tu") || question.contains("jsi tady")) {
			if (!sent1.contains(message.getSender())) {
				sent1.add(message.getSender());
				responses.add(notHere);
				return;
			}
		} else if (question.contains("kdo jsi") || question.contains("kdo je to") || question.contains("kdo je tam") || question.contains("kdo pise") || question.contains("kdo odpovida") || question.contains("kdo to pise") || question.contains("kdo to teda pise")) {
			if (!sent2.contains(message.getSender())) {
				sent2.add(message.getSender());
				responses.add(notMe);
				return;
			}
		} else if (question.contains("spis")) {
			if (!sent3.contains(message.getSender())) {
				sent3.add(message.getSender());
				responses.add(notAsleep);
				return;
			}
		} else if (question.equals("jsi to ty?")) {
			sent3.add(message.getSender());
			responses.add("Ne :/");
			return;
		}
	}

}

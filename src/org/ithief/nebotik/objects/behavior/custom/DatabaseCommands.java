package org.ithief.nebotik.objects.behavior.custom;

import org.ithief.nebotik.database.Database;
import org.ithief.nebotik.database.reflective.DumpEntry;
import org.ithief.nebotik.objects.Message;
import org.ithief.nebotik.ui.MessageServiceUI;

public class DatabaseCommands extends BasicCommands {

	private Database db;

	public DatabaseCommands(MessageServiceUI c) {
		super(c);
		this.db = c.getDB();
	}

	@Override
	protected String getResponseForBasicCommand(Message message, String command, String... params) {
		String[] nparams;
		String ncommand = "";
		if (params.length > 1) {
			nparams = new String[params.length - 1];
			for (int i = 1; i < params.length; i++) {
				nparams[i - 1] = params[i];
			}
		} else {
			nparams = new String[0];
		}
		if (params.length > 0) {
			ncommand = params[0];
		} else {
			ncommand = "";
		}
		switch (command) {
		default:
			return null;
		case "db":
			return getResponseForDBCommand(message, ncommand, nparams);
			
		}
	}

	private String getResponseForDBCommand(Message message, String command, String... params) {
		switch (command) {
		default:
			return null;
		case "help":
			return "P��kazy: add, remove, list, dump";
		case "add":
			return "Tohle ti nem��u povolit";
		case "remove":
			return "Tohle ti rad�ji nedovol�m";
		case "list":
			return getDBList();
		case "dump":
			return "Obsah tabulek ti uk�zat nesm�m :(";
		case "":
			return "Pro seznam p��kaz� p�idej je�t� \"help\" :)";
		}
	}

	private String getDBList() {
		DumpEntry[] dump = db.dumpAll();
		StringBuilder sb = new StringBuilder();
		for (DumpEntry dmp : dump) {
			sb.append(", " + dmp.getKey());
		}
		String res = sb.toString();
		if (res.startsWith(", ")) {
			res = res.substring(2) + ".";
		} else {
			return "Nem�m tady ��dnou tabulku :(";
		}
		return "Seznam tabulek: " + res;
	}

}

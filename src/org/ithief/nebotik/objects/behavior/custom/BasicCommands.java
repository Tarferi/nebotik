package org.ithief.nebotik.objects.behavior.custom;

import java.util.List;

import org.ithief.nebotik.objects.Message;
import org.ithief.nebotik.objects.behavior.CommandListener;
import org.ithief.nebotik.ui.MessageServiceUI;

public class BasicCommands implements CommandListener {

	public BasicCommands(MessageServiceUI c) {
	}

	protected String getResponseForBasicCommand(Message message, String command, String... params) {
		switch (command) {
		default:
			return null;
		case "help":
			return "Pomo� si s�m :)\nP��kazy m�m zat�m nap� @db, @say a @test";
		case "?":
			return "Co t�m chce� ��ct?";
		case "say":
			return "Pr�v� te� se mi nic opakovat nechce :)";
		case "test":
			return "Tohle d�lat nem��e�. Kdyby si takhle testoval ka�d�, tak se z toho zbl�zn�me.";
		}
	}

	@Override
	public final void getResponseForCommand(List<String> responses, Message message, String command, String... params) {
		String rp = "[" + command + "] -> ";
		String resp = getResponseForBasicCommand(message, command, params);
		if (resp != null) {
			responses.add(rp + resp);
		}

	}

}

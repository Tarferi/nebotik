package org.ithief.nebotik.objects.behavior.custom;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ithief.nebotik.database.SenderDatabase;
import org.ithief.nebotik.objects.Message;
import org.ithief.nebotik.objects.behavior.Behavior;
import org.ithief.nebotik.objects.behavior.CommandListener;
import org.ithief.nebotik.objects.behavior.MessageListener;
import org.ithief.nebotik.ui.MessageServiceUI;
import org.ithief.nebotik.ui.ServiceUI;

public class MessageReplier implements Behavior {

	private MessageServiceUI client;

	private List<CommandListener> commands = new ArrayList<>();
	private List<MessageListener> messages = new ArrayList<>();

	private SenderDatabase ignored;

	public MessageReplier(MessageServiceUI c) {
		this.client = c;
		commands.add(new BasicCommands(c));
		commands.add(new ExtendedCommands(c));
		commands.add(new DatabaseCommands(c));
		messages.add(new BasicMessages(c));
		ignored = new SenderDatabase(c.getDB(), "MessageReplier_ignored");
	}

	private String getUnknownCommandResponse(Message message, String command, String... params) {
		String cnt = "Tak tohle fakt neum�m :/";
		return "[" + command + "] -> " + cnt;
	}

	private String getUnknownMessageResponse(Message message) {
		return null; // When no response it given from code, do not respond
	}

	public static final String stripAccents(String s) {
		s = Normalizer.normalize(s, Normalizer.Form.NFD);
		s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return s;
	}

	private void getResponse(List<String> responses, MessageServiceUI ui, Message message) {
		if (ignored.contains(message.getSender()) && !message.getContents().equals("@unignoreme")) {
			return;
		}
		if (message.getContents().startsWith("@")) {
			handleCommand(responses, ui, message);
		} else {
			handleMessage(responses, ui, message);
		}
	}

	private void handleMessage(List<String> responses, MessageServiceUI ui, Message message) {
		if (!ui.messagesAreEnabled()) { // Messages are disabled, return null
			return;
		}
		for (MessageListener callback : messages) {
			callback.getResponseForMessage(responses, message);
		}
		if (responses.size() == 0) {
			String m = getUnknownMessageResponse(message);
			if (m != null) {
				responses.add(m);
			}
			return;
		}
	}

	private void handleCommand(List<String> responses, MessageServiceUI ui, Message message) {
		if (!ui.commandsAreEnabled()) { // Commands are disabled, return null
			return;
		}
		String[] data = message.getContents().split(" ");
		if (data.length == 0) { // Not a message
			return;
		}
		if (!data[0].startsWith("@")) { // Not a command
			return;
		}
		if (data[0].length() <= 1) { // Only "@" character, also not a command
			return;
		}
		String op = data[0].substring(1).toLowerCase();
		if (op.equalsIgnoreCase("ignoreme")) {
			if (!ignored.contains(message.getSender())) {
				ignored.add(message.getSender());
				System.out.println("Adding " + message.getSender() + " to ignored list as per request.");
				responses.add("[" + op + "] -> Jak si p�eje� :(");
				return;
			}
		} else if (op.equalsIgnoreCase("unignoreme")) {
			if (ignored.contains(message.getSender())) {
				ignored.remove(message.getSender());
				System.out.println("Removing " + message.getSender() + " from ignored list as per request");
				responses.add("[" + op + "] -> Jak si p�eje� :)");
				return;
			}
			responses.add("[" + op + "] -> J� t� ale neignoruji :/");
			return;
		}
		String[] params = new String[data.length - 1];
		for (int i = 1; i < data.length; i++) {
			params[i - 1] = data[i];
		}
		for (CommandListener callback : commands) {
			callback.getResponseForCommand(responses, message, op, params);
		}
		if (responses.size() == 0) {
			String m = getUnknownCommandResponse(message, op, params);
			if (m != null) {
				responses.add(m);
			}
			return;
		}
	}

	/**
	 * This cannot be created on heap over and over again.
	 */
	private final List<String> responses = new ArrayList<>();

	private void handleUnreadMessage(MessageServiceUI ui, Message msg) {
		responses.clear();
		getResponse(responses, ui, msg);
		if (responses.isEmpty()) {
			if (ui.isReadMessageEnabled()) { // If reading is enabled
				client.readMessage(msg);
			}
		} else {
			for (String response : responses) {
				client.replyTo(msg, response);
			}
		}
	}

	private final Map<String, String> lastMessages = new HashMap<>();

	private boolean messageInCache(Message msg) {
		if (lastMessages.containsKey(msg.getSender())) {
			String str = lastMessages.get(msg.getSender());
			return str.equals(msg.getAsString());
		}
		return false;
	}

	private void addMessageToCache(Message msg) {
		lastMessages.put(msg.getSender(), msg.getAsString());
	}

	private List<Message> getTrulyUnreadMessages(List<Message> msgs) {
		for (Message m : msgs) {
			if (!messageInCache(m)) {
				addMessageToCache(m);
			} else {
				m.setOld(true);
			}
		}
		return msgs;
	}

	@Override
	public boolean behave(ServiceUI ui, boolean enabled, boolean outputMessages) {
		ui.handleStatusChanged("Reading messages");
		try {
			List<Message> msgs = getTrulyUnreadMessages(client.getMessages(false));
			if (!enabled) { // When disabled, it only reads messages but won't parse anything
				return true;
			}
			for (Message msg : msgs) {
				if (!msg.isRead()) {
					if (!msg.isOld()) {
						if (outputMessages) {
							ui.handleMessage(msg.toString());
						}
					}
					System.out.println(""); // Line separator
					handleUnreadMessage((MessageServiceUI) ui, msg);
					int count = msg.getUnreadCount();
					String s = "Have " + count + " unread message" + (count > 1 ? "s" : "") + " from " + msg.getSender() + " ";
					System.out.print(s);
					if (msg.hasBeenMinutes()) {
						System.out.println("but it's been only minutes.");
					} else if (msg.hasBeenHours()) {
						System.out.println("but it's been only hours.");
					} else if (msg.hasBeenWeek()) {
						System.out.println("but it's been only less than 7 days.");
					} else if (msg.hasBeenDays()) {
						System.out.println("but it's been only days.");
					}
					System.out.println(""); // Line separator
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}

package org.ithief.nebotik.objects.behavior;

import org.ithief.nebotik.ui.ServiceUI;

public interface Behavior {

	public boolean behave(ServiceUI ui, boolean enabled, boolean outputMessages);

}

package org.ithief.nebotik.objects;

public interface Message {

	public String getSender();

	public String getContents();
	
	public boolean isValid();

	public String getAsString();

	public void setOld(boolean b);

	public boolean isRead();

	public boolean isOld();

	public int getUnreadCount();

	public boolean hasBeenMinutes();

	public boolean hasBeenHours();

	public boolean hasBeenWeek();

	public boolean hasBeenDays();

	public String getID();

}

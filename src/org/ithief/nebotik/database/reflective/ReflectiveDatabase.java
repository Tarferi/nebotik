package org.ithief.nebotik.database.reflective;

import java.util.Map;
import java.util.Map.Entry;

import org.mapdb.DB;

public class ReflectiveDatabase {

	private DB db;

	public ReflectiveDatabase(DB db) {
		this.db = db;
		/*
		 * Map<String, Object> all = db.getAll(); for (Entry<String, Object> al : all.entrySet()) { String key = al.getKey(); Object value = al.getValue(); if (value instanceof HTreeMap.KeySet<?>) { HTreeMap.KeySet<?> val = (KeySet<?>) value; Map<?, ?> map = val.getMap(); System.out.println("B"); } System.out.println("A"); }
		 */
	}

	public DumpEntry dump(String tableName) {
		Object value = db.get(tableName);
		System.out.println(tableName + ": " + value);
		return new DumpEntry(tableName, value.toString());
	}

	public DumpEntry[] dumpAll() {
		Map<String, Object> all = db.getAll();
		DumpEntry[] res = new DumpEntry[all.size()];
		int i = 0;
		for (Entry<String, Object> al : all.entrySet()) {
			res[i] = dump(al.getKey());
			i++;
		}
		return res;
	}

}

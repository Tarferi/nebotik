package org.ithief.nebotik.database.reflective;

public class DumpEntry {
	private String key;
	private String value;

	protected DumpEntry(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

}
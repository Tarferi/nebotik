package org.ithief.nebotik.database;

import java.util.Set;

public class SenderDatabase {

	private Set<String> data;
	private Database db;

	@SuppressWarnings("unchecked")
	public SenderDatabase(Database db, String file) {
		this.db = db;
		data = (Set<String>) db.getSet("SenderDatabase_" + file);
	}

	public void add(String dt) {
		data.add(dt);
	}

	public boolean contains(String dt) {
		return data.contains(dt);
	}

	public void remove(String dt) {
		data.remove(dt);
	}

	public String getFirstEntry() {
		if (data.size() > 0) {
			return data.iterator().next();
		} else {
			return null;
		}
	}

	public void clear() {
		data.clear();
	}

	public void save() {
		db.commit();
	}

}

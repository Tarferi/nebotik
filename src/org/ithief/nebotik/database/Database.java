package org.ithief.nebotik.database;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import org.ithief.nebotik.database.reflective.DumpEntry;
import org.ithief.nebotik.database.reflective.ReflectiveDatabase;
import org.ithief.nebotik.ui.MainUI;
import org.mapdb.DB;
import org.mapdb.DBException.DataCorruption;
import org.mapdb.DBMaker;
import org.mapdb.DBMaker.Maker;

public class Database {

	private final DB db;
	private final ReflectiveDatabase rdb;

	private Map<String, Object> loadedObjects = new HashMap<>();
	private MainUI ui;

	public Database(MainUI ui, String DatabaseFile) {
		this.ui = ui;
		DB _db = null;
		Maker store = DBMaker.fileDB(DatabaseFile);
		store.transactionEnable();
		try {
			_db = store.make();
		} catch (DataCorruption e) { // Store not closed properly?
			if (_db != null) {
				_db.close();
			}
			_db = store.checksumHeaderBypass().make();
			_db.commit();
		}
		store.closeOnJvmShutdown();
		db = _db;
		rdb = new ReflectiveDatabase(db);
	}

	public DumpEntry dump(String name) {
		return rdb.dump(name);
	}

	public DumpEntry[] dumpAll() {
		return rdb.dumpAll();
	}

	@SuppressWarnings("rawtypes")
	public ConcurrentMap gethMap(String name) {
		if (loadedObjects.containsKey(name)) {
			return (ConcurrentMap) loadedObjects.get(name);
		} else {
			@SuppressWarnings({ "deprecation" })
			ConcurrentMap map = db.hashMap(name).make();
			loadedObjects.put(name, map);
			return map;
		}
	}

	@SuppressWarnings("rawtypes")
	public Set<?> getSet(String name) {
		if (loadedObjects.containsKey(name)) {
			return (Set) loadedObjects.get(name);
		} else {
			@SuppressWarnings({ "deprecation" })
			Set set = db.hashSet(name).make();
			loadedObjects.put(name, set);
			return set;
		}
	}

	public void close() {
		db.close();
	}

	public class DatabaseException extends Exception {

		private static final long serialVersionUID = 458900084486637847L;

	}

	private final AsyncSaver saver = new AsyncSaver();

	public void commit() {
		saver.addSaveRequest();
	}

	private class AsyncSaver {

		private int saveCountRequests = 0;

		private final Object syncer=new Object();
		private boolean loop = true;

		protected AsyncSaver() {
			thread.start();
		}

		protected void addSaveRequest() {
			synchronized (syncer) {
				saveCountRequests++;
				if (saveCountRequests == 1) { // Thread is sleeping
					syncer.notify(); // Wake up the thread
				}
			}
		}

		private Thread thread = new Thread() {

			@Override
			public void run() {
				setName("Database async save thread");
				loop();
			}
		};

		private void loop() {
			while (loop) {
				synchronized (syncer) {
					if (saveCountRequests == 0) { // No jobs for me at the moment, go to sleep
						try {
							syncer.wait();
						} catch (InterruptedException e) {
						}
					}
					// Awaken by external event
					if (saveCountRequests <= 0) { // Error -> quit
						return;
					}
					// There are some requests to fulfill
					saveCountRequests--; // Subtract 1 request to fulfill
				}
				commit();
			}
		}

		private void commit() {
			ui.setDBShowing(true);
			db.commit();
			ui.setDBShowing(false);
		}

	}
}

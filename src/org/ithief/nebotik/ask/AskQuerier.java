package org.ithief.nebotik.ask;

import java.awt.Color;
import java.util.List;

import org.ithief.nebotik.ui.ServiceStatus;
import org.ithief.nebotik.ui.ServiceUI;

public class AskQuerier {

	private Thread thr = new Thread() {

		@Override
		public void run() {
			setName("Ask.fm query service");
			handle();
		}
	};
	private AskClient hc;
	private ServiceUI uc;

	private final Object syncer = new Object();

	private boolean loop = true;

	private int error_count = 0;

	private static final int max_errors = 5; // max 5 errors

	private static final int error_delay = 30 * 60; // 30 minutes

	private void handle() {
		hc.init();
		uc.setStatus(ServiceStatus.Online);
		uc.handleStatusChanged("Starting");
		while (loop) {
			process();
			uc.handleStatusChanged("Waiting for 2 minues, no error");
			sleep(120);
			if (!loop) {
				break;
			}
		}
		uc.handleStatusChanged("Exiting");
		uc.setStatus(ServiceStatus.Offline);
	}

	private void sleep(int seconds) {
		synchronized (syncer) {
			try {
				uc.setStatus(ServiceStatus.Waiting);
				syncer.wait(seconds * 1000); // Check every 2 minutes
				if (uc.getStatus() != ServiceStatus.Stopping) {
					uc.setStatus(ServiceStatus.Online);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void process() {
		uc.handleStatusChanged("Updating Ask.fm");
		int count = hc.getUnreadNotifications();
		error_count++;
		if (count < 0) { // Error
			List<Exception> e = hc.getExceptions();
			if (e.size() > 0) {
				uc.handleExceptions(e);
				uc.setTextOnMainScreen("A!", Color.red);
				uc.handleStatusChanged("There was an error");
			} else { // Unknown state
				uc.handleStatusChanged("There was unknown error");
				uc.setTextOnMainScreen("A?", Color.orange);
			}
		} else if (count == 0) { // No new messages
			uc.setTextOnMainScreen("", Color.white);
			error_count = 0;
		} else { // New messages
			uc.setTextOnMainScreen(count + "", Color.yellow);
			error_count = 0;
		}
		uc.handleNewError(error_count);
		if (error_count != 0) {
			uc.handleStatusChanged("There was an error, waiting for " + error_delay + " seconds");
			sleep(error_delay);
		}
		if (error_count > max_errors) {
			uc.setTextOnMainScreen("!", Color.red);
			stop();
		}
		uc.handleStatusChanged("Updating Ask.fm finished");
	}

	public void asyncNotify() {
		process();
	}

	public AskQuerier(AskClient c, ServiceUI ui) {
		this.hc = c;
		this.uc = ui;
		thr.start();
	}

	@SuppressWarnings("deprecation")
	public void stop() {
		uc.setStatus(ServiceStatus.Stopping);
		loop = false;
		forceUpdate();
		thr.stop(); // Kill it with fire
		uc.setStatus(ServiceStatus.Offline);
	}

	public void forceUpdate() {
		try {
			synchronized (syncer) {
				syncer.notifyAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

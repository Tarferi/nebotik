package org.ithief.nebotik.ask;

import java.util.ArrayList;
import java.util.List;

import org.ithief.nebotik.raw.ErrorClient;
import org.ithief.nebotik.raw.HTTPClient;

import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlCheckBoxInput;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class AskConnector extends ErrorClient {

	private HTTPClient hc;

	public AskConnector() {
		this.hc = new HTTPClient();
	}

	public void init() {
		hc.init();		
	}
	
	public boolean isStillLogged(boolean forceUpdate) {
		try {
			boolean check = forceUpdate;
			if (hc.getLastPage() == null) {
				check = true;
			}
			if (check) {
				hc.updatePage("https://ask.fm/login");
			}
			if (hc.getLastPage() == null) {
				return false;
			}
			return hc.getLastPage().asXml().contains("/account/wall");
		} catch (Exception e) {
			e.printStackTrace();
			setError(e);
			return false;
		}
	}

	@Override
	public boolean hasError() {
		boolean hce = hc.hasError();
		if (hce) { // If raw client has error, we have error
			return true;
		} else {
			return super.hasError();
		}
	}

	@Override
	public List<Exception> getExceptions() {
		List<Exception> list = new ArrayList<>();
		list.addAll(super.getExceptions());
		list.addAll(hc.getExceptions());
		return list;
	}

	@Override
	public void clearError() {
		hc.clearError();
		super.clearError();
	}

	public boolean login() {
		try {
			hc.updatePage("https://ask.fm/login");
			if (hc.getLastPage() == null) {
				setError(new Exception("Failed to get login page"));
				return false;
			}
			HtmlTextInput username = hc.getLastPage().getElementByName("login");
			HtmlPasswordInput password = hc.getLastPage().getElementByName("password");
			HtmlCheckBoxInput rememberMe = hc.getLastPage().getElementByName("remember_me");
			username.setValueAttribute(AskSettings.Username);
			password.setValueAttribute(AskSettings.Password);
			rememberMe.setChecked(true);
			DomNodeList<DomElement> loginBtns = hc.getLastPage().getElementsByTagName("input");
			for (DomElement e : loginBtns) {
				if (e.getAttribute("class").equals("btn-primary-wide")) {
					if (e instanceof HtmlSubmitInput) {
						HtmlSubmitInput loginBtn = (HtmlSubmitInput) e;
						hc.updatePage(loginBtn);
						return isStillLogged(false);
					}
				}
			}
			return false;
		} catch (Exception e) {
			setError(e);
			return false;
		}
	}

	public int getUnreadNotifications() {
		try {
			DomElement tm = hc.getLastPage().getElementById("topMenu").getFirstElementChild().getFirstElementChild().getFirstElementChild().getNextElementSibling();
			DomNodeList<HtmlElement> els = tm.getElementsByTagName("span");
			for (HtmlElement e : els) {
				if (e.getAttribute("class").equals("notificationCounter")) {
					String str = e.asText();
					try {
						int c = Integer.parseInt(str);
						return c;
					} catch (Exception ee) {
					}
				}
			}
			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			setError(e);
		}
		return 0;
	}

	public void updateCookie(String string, String cookie) {
		hc.updateCookie("ask.fm", string, cookie);
	}
}

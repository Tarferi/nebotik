package org.ithief.nebotik.ask;

import java.util.List;

import org.ithief.nebotik.ui.ServiceStatus;
import org.ithief.nebotik.ui.ServiceUI;

public class AskClient {

	private AskConnector hc;
	private AskQuerier querier;

	public AskClient(ServiceUI ui) {
		ui.setStatus(ServiceStatus.Starting);
		ui.handleStatusChanged("Starting...");
		this.hc = new AskConnector();
		this.querier = new AskQuerier(this, ui);
	}
	
	protected void init() {
		hc.init();
		hc.updateCookie("_m_ask_fm_session", AskSettings.Cookie);
	}

	public int getUnreadNotifications() {
		hc.clearError();
		if (!hc.isStillLogged(true)) {
			if (!hc.login()) {
				return -1;
			} else {
				if (!hc.isStillLogged(false)) {
					return -1;
				}
			}
		}
		int c = hc.getUnreadNotifications();
		return c;
	}

	public List<Exception> getExceptions() {
		return hc.getExceptions();
	}

	public void stop() {
		querier.stop();
	}

	public void forceUpdate() {
		querier.forceUpdate();
	}

}

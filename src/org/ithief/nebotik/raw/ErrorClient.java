package org.ithief.nebotik.raw;

import java.util.ArrayList;
import java.util.List;

public class ErrorClient {

	private boolean has_error = false;

	public void setError(Exception e) {
		has_error = true;
		if (e != null) {
			lst.add(e);
		}
	}

	public void clearError() {
		has_error = false;
		lst.clear();
	}

	public boolean hasError() {
		return has_error;
	}

	public List<Exception> getExceptions() {
		List<Exception> list = new ArrayList<>();
		list.addAll(lst);
		return list;
	}

	private final List<Exception> lst = new ArrayList<>();

}

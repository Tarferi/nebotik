package org.ithief.nebotik.raw;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;

import org.apache.commons.logging.LogFactory;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;

public class HTTPClient extends ErrorClient {

	private WebClient wc;

	protected HtmlPage lastPage;

	private static final String cookieFile = "cookies.txt";

	protected void debugInfo(String s) {

	}

	public HtmlPage getLastPage() {
		return lastPage;
	}

	public void init() {
		java.util.logging.Logger.getLogger("org.apache.http.client.protocol.ResponseProcessCookies processCookies").setLevel(Level.OFF);
		LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
		wc = new WebClient(BrowserVersion.CHROME);
		wc.getOptions().setJavaScriptEnabled(false);
		wc.getOptions().setThrowExceptionOnScriptError(false);
		wc.getOptions().setRedirectEnabled(true);
		// wc.setAjaxController(new NicelyResynchronizingAjaxController());
		wc.getCookieManager().setCookiesEnabled(true);
		loadCookies();
	}

	private long lastRequest = 0;

	public long getLastRequest() {
		return lastRequest;
	}

	public void updatePage(String url) {
		lastRequest = System.currentTimeMillis() / 1000;
		// System.out.println("Opening: " + url);
		try {
			lastPage = wc.getPage(url);
		} catch (Exception e) {
			e.printStackTrace();
			setError(e);
		}
		saveCookies();
	}

	public void updatePage(DomElement el) {
		try {
			lastPage = el.click();
		} catch (IOException e) {
			setError(e);
		}
	}

	private void saveCookies() {
		ObjectOutput out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream(cookieFile));
			out.writeObject(wc.getCookieManager().getCookies());
			out.close();
		} catch (Exception e) {
			setError(e);
		}
		try {
			out.close();
		} catch (Exception e) {
			setError(e);
		}
	}

	private void loadCookies() {
		ObjectInputStream in = null;
		try {
			File file = new File(cookieFile);
			if (file.exists()) {
				in = new ObjectInputStream(new FileInputStream(file));
				@SuppressWarnings("unchecked")
				Set<Cookie> cookies = (Set<Cookie>) in.readObject();
				in.close();
				wc.getCookieManager().clearCookies();
				Iterator<Cookie> i = cookies.iterator();
				while (i.hasNext()) {
					wc.getCookieManager().addCookie(i.next());
				}
			}
		} catch (Exception e) {
			setError(e);
		}
		try {
			in.close();
		} catch (Exception e) {
			setError(e);
		}
	}

	public void close() {
		saveCookies();
		wc.close();
	}

	public void updateCookie(String domain, String key, String cookie) {
		Cookie c = wc.getCookieManager().getCookie(key);
		if (c != null) {
			Date d = c.getExpires();
			Cookie cc;
			if (d != null) {
				cc = new Cookie(c.getDomain(), c.getName(), cookie, c.getPath(), d, c.isSecure());
			} else {
				cc = new Cookie(c.getDomain(), c.getName(), cookie);
			}
			wc.getCookieManager().removeCookie(c);
			wc.getCookieManager().addCookie(cc);
		} else {
			Date d = new Date();
			d.setTime(new Date().getTime() + 7 * 24 * 60 * 60 * 1000);
			Cookie cc = new Cookie(domain, key, cookie, "/", d, false);
			wc.getCookieManager().addCookie(cc);
		}
	}

}

package org.ithief.nebotik.ui;

import java.util.List;

import org.ithief.nebotik.objects.Message;

public interface MessageServiceUI extends ServiceUI {

	public boolean messagesAreEnabled();

	public boolean commandsAreEnabled();

	public void readMessage(Message msg);

	public void replyTo(Message msg, String response);

	public boolean isReadMessageEnabled();

	public List<Message> getMessages(boolean update);

	public void processMessage(Message msg);

	public boolean behaviorIsEnabled();

}

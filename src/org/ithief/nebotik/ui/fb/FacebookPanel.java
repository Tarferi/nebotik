package org.ithief.nebotik.ui.fb;

import net.miginfocom.swing.MigLayout;

import java.awt.GridBagLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import org.ithief.nebotik.database.Database;
import org.ithief.nebotik.database.SenderDatabase;
import org.ithief.nebotik.facebook.FacebookClient;
import org.ithief.nebotik.facebook.objects.FacebookMessage;
import org.ithief.nebotik.objects.Message;
import org.ithief.nebotik.ui.Executor;
import org.ithief.nebotik.ui.MainUI;
import org.ithief.nebotik.ui.MessageServiceUI;
import org.ithief.nebotik.ui.MyRunnable;
import org.ithief.nebotik.ui.main.CommonServicePanel;
import org.ithief.nebotik.ui.my.MyButton;
import org.ithief.nebotik.ui.my.MyLabel;
import org.ithief.nebotik.ui.my.MyPanel;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class FacebookPanel extends CommonServicePanel implements MessageServiceUI {

	private boolean messagesEnabled = false;
	private boolean commandsEnabled = false;
	private boolean behaviorEnabled = false;
	private boolean runEnabled = false;
	private boolean readEnabled = false;
	private MyLabel lblBhv;
	private MyButton btnBhv;
	private MyLabel lblMsg;
	private MyButton btnMsg;
	private MyLabel lblCmd;
	private MyButton btnCmd;
	protected int myErrors;
	private MyLabel lblUpd;
	private MyLabel lblErr;
	private MyLabel lblRun;
	private MyButton btnRun;
	private MyLabel lblCop;
	private MyLabel lblRd;
	private MyButton btnRd;

	private static final String msgStr = "MessagesEnabled";
	private static final String bhvStr = "BehaviorEnabled";
	private static final String runStr = "RunEnabled";
	private static final String cmdStr = "CommandsEnabled";
	private static final String rdStr = "ReadEnabled";

	private FacebookClient client;

	public boolean behaviorIsEnabled() {
		return behaviorEnabled;
	}

	public boolean commandsAreEnabled() {
		return commandsEnabled;
	}

	public boolean messagesAreEnabled() {
		return messagesEnabled;
	}

	public boolean isReadMessageEnabled() {
		return readEnabled;
	}

	private void update(boolean en, MyLabel lbl, MyButton btn) {
		if (en) {
			btn.setText("Disable");
			lbl.setText("Enabled");
			lbl.setForeground(Color.green);
		} else {
			btn.setText("Enable");
			lbl.setText("Disabled");
			lbl.setForeground(Color.red);
		}
	}

	public void setBehaviorEnabled(boolean en) {
		this.behaviorEnabled = en;
		update(en, lblBhv, btnBhv);
		validateSettings();
	}

	public void setCommandsEnabled(boolean en) {
		this.commandsEnabled = en;
		update(en, lblCmd, btnCmd);
		validateSettings();
	}

	public void setMessagesEnabled(boolean en) {
		this.messagesEnabled = en;
		update(en, lblMsg, btnMsg);
		validateSettings();
	}

	public void setFacebookEnabled(boolean en) {
		this.runEnabled = en;
		update(en, lblRun, btnRun);
		validateSettings();
		if (runEnabled) { // Enabled client
			if (client != null) { // Already running
				return;
			} else {
				client = new FacebookClient(this);
			}
		} else { // Disable client
			if (client == null) { // Client not running
				return;
			} else {
				client.close();
				client = null;
			}
		}
	}

	public void setReadEnabled(boolean en) {
		this.readEnabled = en;
		update(en, lblRd, btnRd);
		validateSettings();
	}

	public void setLastUpdated(String lastUpdated) {
		lblUpd.setText(lastUpdated);
	}

	private final MainUI ui;

	public FacebookPanel(final MainUI ui) {
		super(ui, "Facebook");
		super.setIcon("/org/ithief/nebotik/ui/fb/icon.png");
		this.ui = ui;
		this.db = new SenderDatabase(ui.getDB(), "FacebookSettings");
		this.commandsEnabled = db.contains(cmdStr);
		this.behaviorEnabled = db.contains(bhvStr);
		this.messagesEnabled = db.contains(msgStr);
		this.runEnabled = db.contains(runStr);
		this.readEnabled = db.contains(rdStr);

		setName("Facebook");
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		MyPanel pnlInfo = new MyPanel();
		GridBagConstraints gbc_pnlInfo = new GridBagConstraints();
		gbc_pnlInfo.insets = new Insets(0, 0, 5, 0);
		gbc_pnlInfo.fill = GridBagConstraints.BOTH;
		gbc_pnlInfo.gridx = 0;
		gbc_pnlInfo.gridy = 0;
		add(pnlInfo, gbc_pnlInfo);
		pnlInfo.setLayout(new MigLayout("", "[][][grow][]", "[::25][::25][]"));

		MyLabel mlblLastUpdate = new MyLabel((String) null);
		mlblLastUpdate.setText("Last update:");
		pnlInfo.add(mlblLastUpdate, "cell 0 0,alignx right");

		lblUpd = new MyLabel((String) null);
		lblUpd.setText("");
		pnlInfo.add(lblUpd, "cell 1 0");

		MyButton mbtnUpdate = new MyButton((String) null);
		mbtnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				forceUpdate();
			}
		});
		mbtnUpdate.setText("Update");
		pnlInfo.add(mbtnUpdate, "cell 3 0,alignx center,growy");

		MyLabel mlblTotalErrors = new MyLabel((String) null);
		mlblTotalErrors.setText("Total errors:");
		pnlInfo.add(mlblTotalErrors, "cell 0 1,alignx right");

		lblErr = new MyLabel((String) null);
		lblErr.setText("");
		pnlInfo.add(lblErr, "cell 1 1");

		MyButton mbtnClear = new MyButton((String) null);
		mbtnClear.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				myErrors = 0;
				handleNewError(0);
			}
		});
		mbtnClear.setText("Clear");
		pnlInfo.add(mbtnClear, "cell 3 1,grow");

		MyLabel mlblCurrentOperation = new MyLabel((String) null);
		mlblCurrentOperation.setText("Current operation:");
		pnlInfo.add(mlblCurrentOperation, "cell 0 2,alignx right");

		lblCop = new MyLabel((String) null);
		lblCop.setText("");
		pnlInfo.add(lblCop, "cell 1 2 3 1");

		MyPanel pnlControl = new MyPanel();
		GridBagConstraints gbc_pnlControl = new GridBagConstraints();
		gbc_pnlControl.fill = GridBagConstraints.BOTH;
		gbc_pnlControl.gridx = 0;
		gbc_pnlControl.gridy = 2;
		add(pnlControl, gbc_pnlControl);
		pnlControl.setLayout(new MigLayout("", "[][][grow][]", "[::25][::25][::25][::25][::25]"));

		MyLabel mlblRun = new MyLabel((String) null);
		mlblRun.setText("Run:");
		pnlControl.add(mlblRun, "cell 0 0,alignx right");

		lblRun = new MyLabel((String) null);
		lblRun.setText("Enabled");
		pnlControl.add(lblRun, "cell 1 0,alignx left");

		btnRun = new MyButton((String) null);
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runEnabled = !runEnabled;
				setFacebookEnabled(runEnabled);
				save();
			}
		});
		btnRun.setText("Disable");
		pnlControl.add(btnRun, "cell 3 0,grow");

		MyLabel mlblBehavior = new MyLabel((String) null);
		mlblBehavior.setText("Process behavior(s):");
		pnlControl.add(mlblBehavior, "cell 0 1,alignx right");

		lblBhv = new MyLabel((String) null);
		lblBhv.setText("Enabled");
		pnlControl.add(lblBhv, "cell 1 1,alignx left");

		btnBhv = new MyButton((String) null);
		btnBhv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setBehaviorEnabled(!behaviorEnabled);
				save();
			}
		});
		btnBhv.setText("Disable");
		pnlControl.add(btnBhv, "cell 3 1,grow");

		MyLabel mlblMessages = new MyLabel((String) null);
		mlblMessages.setText("Process messages:");
		pnlControl.add(mlblMessages, "cell 0 2,alignx right");

		lblMsg = new MyLabel((String) null);
		lblMsg.setText("Enabled");
		pnlControl.add(lblMsg, "cell 1 2,alignx left");

		btnMsg = new MyButton((String) null);
		btnMsg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setMessagesEnabled(!messagesEnabled);
				save();
			}
		});
		btnMsg.setText("Disable");
		pnlControl.add(btnMsg, "cell 3 2,grow");

		MyLabel mlblRead = new MyLabel((String) null);
		mlblRead.setText("Mark messages as read:");
		pnlControl.add(mlblRead, "cell 0 3,alignx right");

		lblRd = new MyLabel((String) null);
		lblRd.setText("Enabled");
		pnlControl.add(lblRd, "cell 1 3");

		btnRd = new MyButton((String) null);
		btnRd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setReadEnabled(!readEnabled);
				save();
			}
		});
		btnRd.setText("Disable");
		pnlControl.add(btnRd, "cell 3 3,grow");

		MyLabel mlblCommands = new MyLabel((String) null);
		mlblCommands.setText("Execute commands:");
		pnlControl.add(mlblCommands, "cell 0 4,alignx right");

		lblCmd = new MyLabel((String) null);
		lblCmd.setText("Enabled");
		pnlControl.add(lblCmd, "cell 1 4,alignx left");

		btnCmd = new MyButton((String) null);
		btnCmd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCommandsEnabled(!commandsEnabled);
				save();
			}
		});
		btnCmd.setText("Disable");
		pnlControl.add(btnCmd, "cell 3 4,grow");
		load(ui);
	}

	private void load(MainUI ui) {
		setBehaviorEnabled(behaviorEnabled);
		setCommandsEnabled(commandsEnabled);
		setMessagesEnabled(messagesEnabled);
		setFacebookEnabled(runEnabled);
		setReadEnabled(readEnabled);
		if (runEnabled) {
			setFacebookEnabled(true);
		}
	}

	private final SenderDatabase db;

	private void saveStr(boolean b, String str) {
		if (b) {
			if (!db.contains(str)) {
				db.add(str);
			}
		} else {
			if (db.contains(str)) {
				db.remove(str);
			}
		}
	}

	public void setCurrentOperation(String text) {
		lblCop.setText(text);
	}

	private void save() {
		saveStr(commandsEnabled, cmdStr);
		saveStr(messagesEnabled, msgStr);
		saveStr(behaviorEnabled, bhvStr);
		saveStr(runEnabled, runStr);
		saveStr(readEnabled, rdStr);
		db.save();
	}

	private void validateSettings() {
		if (!this.behaviorEnabled) { // If no behavior is enabled, turn off the rest as well 
			if (this.commandsEnabled) {
				this.setCommandsEnabled(false);
			}
			if (this.messagesEnabled) {
				this.setMessagesEnabled(false);
			}
			if (this.isReadMessageEnabled()) {
				this.setReadEnabled(false);
			}
		}
	}

	@Override
	protected void timestamp() {
		if (lblUpd != null) {
			lblUpd.setText(super.getTimestamp());
		}
	}

	private static final long serialVersionUID = -7190079109696132385L;

	@Override
	public void handleNewError(int totalCount) {
		Executor.execute(facebookErrorCounterUpdater, totalCount);
	}

	@Override
	public Database getDB() {
		return ui.getDB();
	}

	@Override
	public void forceUpdate() {
		client.forceUpdate();
	}

	@Override
	public void startService() {
		setFacebookEnabled(true);
	}

	@Override
	public void stopService() {
		setFacebookEnabled(false);
	}

	@Override
	public void handleStatusChanged(String string) {
		Executor.execute(facebookStatusChangedUpdater, string);
	}

	private MyRunnable facebookStatusChangedUpdater = new MyRunnable() {

		@Override
		public void run(Object obj) {
			String ec = (String) obj;
			setCurrentOperation(ec);
			timestamp();
		}
	};

	private MyRunnable facebookErrorCounterUpdater = new MyRunnable() {

		@Override
		public void run(Object obj) {
			int errors = (int) obj;
			myErrors += errors;
			lblErr.setText(myErrors + "");
			timestamp();
		}
	};

	@Override
	public void handleMessage(String message) {
		super.handleMessage(message);
		super.setTextOnMainScreen("M", Color.magenta);
	}

	@Override
	public void readMessage(Message msg) {
		client.getFacebookClient().readMessage((FacebookMessage) msg);
	}

	@Override
	public void replyTo(Message msg, String response) {
		client.getFacebookClient().replyTo((FacebookMessage) msg, response);
	}

	@Override
	public List<Message> getMessages(boolean update) {
		List<Message> msgs = new ArrayList<>();
		List<FacebookMessage> mgs = client.getFacebookClient().getMessages(update);
		for (FacebookMessage msg : mgs) {
			msgs.add(msg);
		}
		return msgs;
	}

	@Override
	public void processMessage(Message msg) {
	}

}

package org.ithief.nebotik.ui;

import java.awt.Color;

public enum ServiceStatus {

	Online("Online", Color.green),
	Offline("Offline", Color.red),
	Starting("Starting", Color.pink),
	Stopping("Stopping", Color.lightGray),
	Waiting("Waiting", Color.yellow);

	private String text;
	private Color color;

	ServiceStatus(String txt, Color color) {
		this.text = txt;
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	@Override
	public String toString() {
		return text;
	}
}

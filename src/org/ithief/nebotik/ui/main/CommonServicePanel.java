package org.ithief.nebotik.ui.main;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.ithief.nebotik.database.Database;
import org.ithief.nebotik.ui.Executor;
import org.ithief.nebotik.ui.MainUI;
import org.ithief.nebotik.ui.MyRunnable;
import org.ithief.nebotik.ui.ServiceStatus;
import org.ithief.nebotik.ui.ServiceUI;
import org.ithief.nebotik.ui.my.MyLabel;
import org.ithief.nebotik.ui.my.MyPanel;

public abstract class CommonServicePanel extends MyPanel implements ServiceUI {

	private static final long serialVersionUID = -8697590777431301626L;

	private final MainStatusDataPanel dataPanel;

	private final MainUI ui;

	public CommonServicePanel(MainUI ui, String name) {
		this.ui = ui;
		dataPanel = new MainStatusDataPanel(ui, name);
		dataPanel.setErrorAcceptListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ui.setError(false);
			}

		});
		dataPanel.setMessageAcceptListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mainLabel.setVisible(false);
			}

		});
		setIcon("/org/ithief/nebotik/ui/main/service.png");
		if (ui != null) {
			ui.getLockStatusPanel().addLabel(mainLabel, true);
			setStatus(ServiceStatus.Offline);
		}
	}

	public void handleExceptions(List<Exception> exceptions) {
		Executor.execute(errorAppender, exceptions);
	}

	@Override
	public void handleError(String error) {
		Executor.execute(errorAppender, error);
	}

	@Override
	public void handleMessage(String message) {
		Executor.execute(messageAppender, message);
	}

	private ServiceStatus lastStatus = ServiceStatus.Offline;

	public ServiceStatus getStatus() {
		return lastStatus;
	}

	@Override
	public void setStatus(ServiceStatus status) {
		lastStatus = status;
		Executor.execute(setStatusRunnable, status);
	}

	@Override
	public Database getDB() {
		return ui.getDB();
	}

	@Override
	public String getName() {
		return dataPanel.getName();
	}

	protected void setIcon(String path) {
		try {
			InputStream stream = getClass().getResourceAsStream(path);
			ImageIcon icon = new ImageIcon(ImageIO.read(stream));
			this.icon.setIcon(icon);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private final MyLabel icon = new MyLabel("");

	@Override
	public MyLabel getIcon() {
		return icon;
	}

	@Override
	public MainStatusDataPanel getDataPanel() {
		return dataPanel;
	}

	protected abstract void timestamp();

	private MyLabel mainLabel = new MyLabel("");

	@Override
	public void setTextOnMainScreen(String text, Color color) {
		Executor.execute(txtMainScreenUpdater, new Object[] { text, color });
	}

	@Override
	public void hideTextOnMainScreen() {
		Executor.execute(txtMainScreenHider);
	}

	private MyRunnable errorAppender = new MyRunnable() {

		@Override
		public void run(Object obj) {
			MainStatusDataPanel status = ui.getStatusPanel();
			if (obj instanceof List) {
				@SuppressWarnings("unchecked")
				List<Exception> exceptions = (List<Exception>) obj;
				for (Exception e : exceptions) {
					status.appendException(e);
					dataPanel.appendException(e);
				}
			} else {
				String s = (String) obj;
				dataPanel.appendException(s);
				status.appendException(s);
			}
			timestamp();
			ui.setError();
		}

	};

	private MyRunnable messageAppender = new MyRunnable() {

		@Override
		public void run(Object obj) {
			String msg = (String) obj;
			MainStatusDataPanel status = ui.getStatusPanel();
			dataPanel.appendMessage(msg);
			status.appendMessage(msg);
			timestamp();
		}

	};

	private MyRunnable setStatusRunnable = new MyRunnable() {

		@Override
		public void run(Object obj) {
			ServiceStatus status = (ServiceStatus) obj;
			if (status == ServiceStatus.Online) {
				ui.getLockStatusPanel().setServiceOnline(CommonServicePanel.this, true);
			} else if (status == ServiceStatus.Offline) {
				ui.getLockStatusPanel().setServiceOnline(CommonServicePanel.this, false);
			}
			dataPanel.setStatus(status.toString(), status.getColor());
			timestamp();
		}

	};

	private MyRunnable txtMainScreenHider = new MyRunnable() {

		@Override
		public void run(Object obj) {
			mainLabel.setVisible(false);
		}

	};

	private MyRunnable txtMainScreenUpdater = new MyRunnable() {

		@Override
		public void run(Object obj) {
			Object[] qobj = (Object[]) obj;
			String text = (String) qobj[0];
			Color color = (Color) qobj[1];
			mainLabel.setVisible(true);
			mainLabel.setText(text);
			mainLabel.setForeground(color);
			timestamp();
		}

	};

	private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM HH:mm:ss");

	private static final String getCurrentTimeStamp() {
		return format.format(new Date());
	}

	public String getTimestamp() {
		return getCurrentTimeStamp();
	}
}

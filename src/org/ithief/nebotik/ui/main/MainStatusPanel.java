package org.ithief.nebotik.ui.main;

import org.ithief.nebotik.ui.my.MyPanel;
import org.ithief.nebotik.ui.my.MyTabbedPane;

import java.awt.BorderLayout;
import javax.swing.JTabbedPane;

public class MainStatusPanel extends MyPanel {
	private static final long serialVersionUID = -7727215264644896668L;
	private MyTabbedPane tabbedPane;

	public MainStatusPanel() {
		setName("Status");
		setLayout(new BorderLayout(0, 0));

		tabbedPane = new MyTabbedPane(JTabbedPane.TOP);
		add(tabbedPane, BorderLayout.CENTER);

	}

	public void addTab(MainStatusDataPanel panel) {
		tabbedPane.addTab(panel.getName(), panel);
	}
}

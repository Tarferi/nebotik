package org.ithief.nebotik.ui.main;

import org.ithief.nebotik.ui.my.MyPanel;
import net.miginfocom.swing.MigLayout;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.ithief.nebotik.ui.MainUI;
import org.ithief.nebotik.ui.my.MyLabel;
import org.ithief.nebotik.ui.my.MyTabbedPane;

public class MainStatusDataPanel extends MyPanel {
	private static final long serialVersionUID = -4368025099242466722L;
	private ConsolePanel pnlMsg;
	private ConsolePanel pnlErr;
	private MyLabel lblStatus;
	private MyLabel mlblStatus;

	/**
	 * @wbp.parser.constructor
	 */
	public MainStatusDataPanel(MainUI ui, String string) {
		this(ui, string, true);
	}

	private final MainUI ui;

	public MainStatusDataPanel(MainUI ui, String string, boolean showStatus) {
		setName(string);
		this.ui = ui;
		MyTabbedPane myTabbedPane = new MyTabbedPane();
		mlblStatus = new MyLabel((String) null);
		lblStatus = new MyLabel((String) null);

		if (showStatus) {
			setLayout(new MigLayout("", "[][][grow]", "[][grow]"));
			mlblStatus.setText("Status:");
			add(mlblStatus, "cell 0 0,alignx right");

			lblStatus.setText("<STATUS>");
			add(lblStatus, "cell 1 0");
			add(myTabbedPane, "cell 0 1 3 1,grow");
		} else {
			setLayout(new MigLayout("", "[][][grow]", "[grow]"));
			add(myTabbedPane, "cell 0 0 3 1,grow");
		}

		pnlMsg = new ConsolePanel();
		myTabbedPane.addTab("Messages", null, pnlMsg, null);

		pnlErr = new ConsolePanel();
		pnlErr.setTextColor(Color.red);

		myTabbedPane.addTab("Exceptions", null, pnlErr, null);
	}

	public void setErrorAcceptListener(ActionListener l) {
		pnlErr.setActionListener(l);
	}

	public void setMessageAcceptListener(ActionListener l) {
		pnlMsg.setActionListener(l);
	}

	public void setStatus(String status, Color statusColor) {
		lblStatus.setText(status);
		lblStatus.setForeground(statusColor);
	}

	public void appendMessage(String text) {
		pnlMsg.append(text);
	}

	public void appendException(Exception exception) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		exception.printStackTrace(pw);
		appendException(sw.toString());
		ui.setError();
	}

	public void appendException(String exception) {
		pnlErr.append(exception);
		ui.setError();
	}

}

package org.ithief.nebotik.ui.main;

/*
import org.ithief.nebotik.facebook.objects.Message;
import org.ithief.nebotik.ui.MainUI;
import org.ithief.nebotik.ui.my.MyLabel;
import net.miginfocom.swing.MigLayout;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTabbedPane;
 */

import org.ithief.nebotik.ui.my.MyPanel;

public class StatusPanel extends MyPanel {

	private static final long serialVersionUID = -2103134888789419210L;
	/*
	private JLabel lblCop;
	private JLabel lblEc;
	
	public StatusPanel(MainUI mainUI) {
		setName("Status");
		setLayout(new MigLayout("", "[][][grow]", "[][][grow]"));
	
		JLabel lblCurrentOperation = new MyLabel("Current operation:");
		add(lblCurrentOperation, "cell 0 0,alignx right");
	
		lblCop = new MyLabel("");
		add(lblCop, "cell 1 0");
	
		JLabel lblErrorCount = new MyLabel("Error count:");
		add(lblErrorCount, "cell 0 1,alignx right");
	
		lblEc = new MyLabel("");
		add(lblEc, "cell 1 1");
	
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		add(tabbedPane, "cell 0 2 3 1,grow");
	
		out = new ConsolePanel();
		out.setActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent e) {
				mainUI.setNewMessage(false);
			}
	
		});
		tabbedPane.addTab("Output", null, out, null);
	
		err = new ConsolePanel();
		tabbedPane.addTab("Errors", null, err, null);
		err.setTextColor(Color.red);
		err.setActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent e) {
				mainUI.setNewError(false);
			}
	
		});
	}
	
	public void errorCountChanged(int newErrorCount) {
		lblEc.setText(newErrorCount + "");
	}
	
	public void currentOperationChanged(String nc) {
		lblCop.setText(nc);
	}
	
	private JTabbedPane tabbedPane;
	private ConsolePanel out;
	private ConsolePanel err;
	
	public void appendMessage(Message msg) {
		String txt = msg.getUnreadCount() + " message" + (msg.getUnreadCount() > 1 ? "s" : "") + " from " + msg.getSender() + ": '" + msg.getContent() + "'";
		appendMessage(txt);
	}
	
	public void writeExceptions(List<Exception> e) {
		if (e.size() > 0) {
			String txt = "";
			for (Exception r : e) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				r.printStackTrace(pw);
				txt += sw.toString() + "\n";
			}
			err.append(txt);
		}
	}
	
	public void appendMessage(String msg) {
		out.append(msg);
	}
	 */
}

package org.ithief.nebotik.ui.main;

import net.miginfocom.swing.MigLayout;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;

import org.ithief.nebotik.ui.MainUI;
import org.ithief.nebotik.ui.ServiceUI;
import org.ithief.nebotik.ui.my.MyButton;
import org.ithief.nebotik.ui.my.MyLabel;
import org.ithief.nebotik.ui.my.MyPanel;
import javax.swing.JProgressBar;
import java.awt.Dimension;
import javax.swing.border.EmptyBorder;

public class LockStatusPanel extends MyPanel {

	private MyLabel lblOnline;
	private MyLabel lblUnlocked;
	private MyButton btnLock;
	private boolean locked;

	private void setOnline(boolean online) {
		if (online) {
			lblOnline.setForeground(Color.green);
			lblOnline.setText("Online");
		} else {
			lblOnline.setForeground(Color.red);
			lblOnline.setText("Offline");
		}
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
		if (locked) {
			lblUnlocked.setForeground(Color.cyan);
			lblUnlocked.setText("Locked");
			btnLock.setText("Unlock");
			btnLock.setVisible(false);
			pgLock.setVisible(false);
		} else {
			lblUnlocked.setForeground(Color.green);
			lblUnlocked.setText("Unlocked");
			btnLock.setText("Lock");
			btnLock.setVisible(true);
			pgLock.setVisible(true);
		}
	}

	public void setDBShowing(boolean showing) {
		this.lblDb.setVisible(showing);
	}

	private final List<MyLabel> icons = new ArrayList<>();

	public void setServiceOnline(ServiceUI ui, boolean online) {
		MyLabel icon = ui.getIcon();
		if (online) {
			if (!icons.contains(icon)) { // This icon is not displayed yet
				icon.setBorder(new EmptyBorder(0, 3, 0, 3));
				pnlOnlines.add(icon);
				icons.add(icon);
				pnlOnlinesWrap.setVisible(true);
				setOnline(true);
			}
		} else {
			if (icons.contains(icon)) { // This icon is displayed
				pnlOnlines.remove(icon);
				icons.remove(icon);
				if (icons.isEmpty()) {
					pnlOnlinesWrap.setVisible(false);
					setOnline(false);
				}
			}
		}
	}

	public LockStatusPanel(final MainUI mainUI) {
		setLayout(new MigLayout("", "[][50][][grow][][][60][][]", "[grow]"));

		MyLabel lblStatus = new MyLabel("Status:");
		add(lblStatus, "cell 0 0,alignx right,aligny center");

		lblOnline = new MyLabel("Online");
		add(lblOnline, "cell 1 0,alignx center,aligny center");

		lblNwErr = new MyLabel("E");
		lblNwErr.setBorder(new EmptyBorder(0, 3, 0, 3));
		lblNwErr.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (!locked) {
					lblNwErr.setVisible(false);
				}
			}
		});

		pnlOnlinesWrap = new MyPanel();
		pnlOnlinesWrap.setVisible(false);
		add(pnlOnlinesWrap);
		pnlOnlinesWrap.setLayout(new BoxLayout(pnlOnlinesWrap, BoxLayout.X_AXIS));

		myLabel = new MyLabel((String) null);
		myLabel.setVisible(false);
		myLabel.setText("(");
		pnlOnlinesWrap.add(myLabel);

		pnlOnlines = new MyPanel();
		pnlOnlinesWrap.add(pnlOnlines);
		pnlOnlines.setLayout(new BoxLayout(pnlOnlines, BoxLayout.X_AXIS));

		myLabel_1 = new MyLabel((String) null);
		myLabel_1.setVisible(false);
		pnlOnlinesWrap.add(myLabel_1);
		myLabel_1.setText(")");

		pnlData = new MyPanel();
		add(pnlData, "cell 3 0,alignx right,aligny center");
		pnlData.setLayout(new BoxLayout(pnlData, BoxLayout.X_AXIS));
		lblNwErr.setForeground(Color.RED);
		add(lblNwErr, "cell 4 0,aligny center");

		lblDb = new MyLabel((String) null);
		lblDb.setBorder(new EmptyBorder(0, 3, 0, 3));
		lblDb.setForeground(Color.ORANGE);
		lblDb.setText("DB");
		add(lblDb, "cell 5 0,aligny center");

		lblUnlocked = new MyLabel("Unlocked");
		add(lblUnlocked, "cell 6 0,alignx center,aligny center");

		btnLock = new MyButton("Lock");
		btnLock.setPreferredSize(new Dimension(100, 23));
		btnLock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!locked) { // Lock
					mainUI.lockRequested();
				}
			}
		});
		add(btnLock, "cell 8 0,grow");

		pnlLock = new MyPanel();
		add(pnlLock, "flowx,cell 7 0,growx");
		pnlLock.setLayout(new BoxLayout(pnlLock, BoxLayout.Y_AXIS));

		lblLast = new MyLabel("");
		pnlLock.add(lblLast);

		pgLock = new JProgressBar();
		pgLock.setVisible(false);
		pgLock.setBackground(Color.BLACK);
		pgLock.setForeground(Color.CYAN);
		pgLock.setValue(300);
		pgLock.setPreferredSize(new Dimension(40, 14));
		pgLock.setMaximumSize(new Dimension(40, 14));
		pgLock.setMaximum(300);
		pnlLock.add(pgLock);

		setOnline(false);
		setLocked(false);
		setNewError(false);
		setDBShowing(false);
	}

	private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM HH:mm:ss");

	private static final String getCurrentTimeStamp() {
		return format.format(new Date());
	}

	public void timestamp() {
		String str = getCurrentTimeStamp();
		lblLast.setText(str);
	}

	private static final long serialVersionUID = -8181290858504939152L;
	private MyLabel lblLast;
	private MyLabel lblNwErr;
	private MyPanel pnlLock;
	private MyLabel lblDb;
	private MyPanel pnlData;
	private MyPanel pnlOnlinesWrap;
	private MyLabel myLabel;
	private MyPanel pnlOnlines;
	private MyLabel myLabel_1;
	private JProgressBar pgLock;

	public void setNewError(boolean nm) {
		lblNwErr.setVisible(nm);
	}

	public void updateLocker(int diff, int max) {
		if (locked) {
			pgLock.setVisible(false);
		} else {
			if (diff > 0 && diff < max - (max / 10)) {
				pgLock.setVisible(true);
				pgLock.setMaximum(max);
				pgLock.setValue(diff);
			} else {
				pgLock.setVisible(false);
			}
		}
	}

	public void addLabel(MyLabel mainLabel, boolean hideOnClick) {
		mainLabel.setBorder(new EmptyBorder(0, 3, 0, 3));
		pnlData.add(mainLabel);
		if (hideOnClick) {
			mainLabel.addMouseListener(new MouseAdapter() {

				@Override
				public void mousePressed(MouseEvent arg0) {
					if (!locked) {
						mainLabel.setVisible(false);
					}
				}

			});
		}
	}

}

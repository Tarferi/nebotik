package org.ithief.nebotik.ui.main;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JTextArea;

import org.ithief.nebotik.ui.MainUI;
import org.ithief.nebotik.ui.my.MyButton;
import org.ithief.nebotik.ui.my.MyPanel;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class ConsolePanel extends MyPanel {
	private JTextArea textArea;
	private ActionListener listenerListener;

	public void setActionListener(ActionListener l) {
		this.listenerListener = l;
	}

	public ConsolePanel() {
		setOpaque(false);
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new MyPanel();
		add(panel, BorderLayout.EAST);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 65, 0 };
		gbl_panel.rowHeights = new int[] { 40, 40, 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JButton btnClear = new MyButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearConsole();
			}
		});
		GridBagConstraints gbc_btnClear = new GridBagConstraints();
		gbc_btnClear.fill = GridBagConstraints.BOTH;
		gbc_btnClear.insets = new Insets(0, 0, 5, 0);
		gbc_btnClear.gridx = 0;
		gbc_btnClear.gridy = 0;
		panel.add(btnClear, gbc_btnClear);

		JButton btnAccept = new MyButton("Accept");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listenerListener != null) {
					listenerListener.actionPerformed(e);
				}
			}
		});
		GridBagConstraints gbc_btnAccept = new GridBagConstraints();
		gbc_btnAccept.insets = new Insets(0, 0, 5, 0);
		gbc_btnAccept.fill = GridBagConstraints.BOTH;
		gbc_btnAccept.gridx = 0;
		gbc_btnAccept.gridy = 1;
		panel.add(btnAccept, gbc_btnAccept);

		JPanel panel_1 = new MyPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 2;
		gbc_panel_1.weightx = 1.0;
		gbc_panel_1.weighty = 1.0;

		panel.add(panel_1, gbc_panel_1);

		textArea = new JTextArea();
		textArea.setForeground(MainUI.foregroundColor);
		textArea.setBackground(MainUI.backgroundColor);
		textArea.setEditable(false);
		textArea.setLineWrap(true);

		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setOpaque(false);
		add(scrollPane, BorderLayout.CENTER);
	}

	public void setTextColor(Color c) {
		textArea.setForeground(c);
	}

	public void clearConsole() {
		textArea.setText("");
		append("Console cleared.");
	}

	private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM HH:mm:ss");

	private static final String getCurrentTimeStamp() {
		return format.format(new Date());
	}

	public void append(String text) {
		String txt = "[" + getCurrentTimeStamp() + "] " + text + "\n";
		textArea.append(txt);
	}

	private static final long serialVersionUID = -7001986000072125004L;

}

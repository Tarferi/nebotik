package org.ithief.nebotik.ui.main;

import net.miginfocom.swing.MigLayout;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import org.ithief.nebotik.database.SenderDatabase;
import org.ithief.nebotik.ui.MainUI;
import org.ithief.nebotik.ui.my.MyButton;
import org.ithief.nebotik.ui.my.MyLabel;
import org.ithief.nebotik.ui.my.MyPanel;

public class LockPanel extends MyPanel {

	private boolean locked;
	private MyButton btnLock;

	private String unlockCode = "";

	public void setLocked(boolean locked) {
		this.locked = locked;
		setCombination("");
		if (locked) {
			btnLock.setText("Unlock");
		} else {
			btnLock.setText("Change code");
		}
	}

	private String getCombination() {
		return txtComb.getText();
	}

	private void setCombination(String combination) {
		txtComb.setText(combination);
	}

	private final MainUI ui;

	private final SenderDatabase code;

	private void setCode(String code) {
		unlockCode = code;
		this.code.clear();
		this.code.add(unlockCode);
		this.code.save();
	}

	public LockPanel(final MainUI ui) {
		this.ui = ui;
		code = new SenderDatabase(ui.getDB(), "Lock_code");
		String c = code.getFirstEntry();
		setCode(c == null ? "" : c);

		setLayout(new MigLayout("", "[right][grow][]", "[34.00][grow]"));

		MyLabel lblCombination = new MyLabel("Combination:");
		lblCombination.setForeground(MainUI.foregroundColor);
		add(lblCombination, "cell 0 0,alignx trailing");

		txtComb = new JTextField();
		txtComb.setForeground(MainUI.foregroundColor);
		txtComb.setOpaque(false);
		txtComb.setEditable(false);
		add(txtComb, "cell 1 0,growx");
		txtComb.setColumns(10);

		MyPanel panel_2 = new MyPanel();
		add(panel_2, "cell 2 0,grow");
		panel_2.setLayout(new MigLayout("", "[57px]", "[][23px]"));

		MyButton btnClear = new MyButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCombination("");
			}
		});

		panel_2.add(btnClear, "cell 0 0,growx,aligny center");

		btnLock = new MyButton("Lock");
		btnLock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (locked) {
					if (getCombination().equals(unlockCode)) {
						ui.unlockRequested();
					} else {
						invalidCode();
					}
				} else {
					setCode(txtComb.getText());
					txtComb.setText("");
					ui.lockRequested();
				}
			}

		});

		panel_2.add(btnLock, "cell 0 1,growx,aligny center");

		MyPanel pnlContent = new MyPanel();
		add(pnlContent, "cell 0 1 3 1,grow");
		pnlContent.setLayout(new GridLayout(4, 3, 0, 0));

		for (int i = 0; i < 9; i++) {
			pnlContent.add(new ValuedButton((i + 1) + ""));
		}
		pnlContent.add(new ValuedButton("*"));
		pnlContent.add(new ValuedButton("0"));
		pnlContent.add(new ValuedButton("#"));
		setLocked(false);
	}

	private void invalidCode() {
		String str = "Tried unlocking with invalid code: '" + getCombination() + "'";
		ui.getStatusPanel().appendException(str);
		System.err.println(str);
	}

	private void codePressed(String code) {
		if (code.equals("#")) {
			if (getCombination().length() > 1) {
				setCombination(getCombination().substring(0, getCombination().length() - 1));
			} else if (getCombination().length() == 1) {
				setCombination("");
			}
		} else if (code.equals("*")) {
			setCombination("");
		} else {
			setCombination(getCombination() + code);
		}
	}

	private static final long serialVersionUID = -7008868191011183006L;
	private JTextField txtComb;

	private class ValuedButton extends MyButton {
		private static final long serialVersionUID = -5812892115350311017L;

		public ValuedButton(final String code) {
			super(code);
			this.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					codePressed(code);
				}

			});
		}
	}
}

package org.ithief.nebotik.ui;

public class LockWorker {

	private static final int secondsBetweenLock = 5 * 60; // 5 minutes to lock due to inactivity

	private static final int secondsBetweenUpdate = 5;

	private Thread thr = new Thread(new Runnable() {

		@Override
		public void run() {
			handle();
		}
	});

	private void handle() {
		while (!thr.isInterrupted()) {
			wait(secondsBetweenUpdate); // Wait 10 seconds, check and go back to sleep
			long lu = System.currentTimeMillis() / 1000;
			long diff = lu - (lastUpdate / 1000);
			if (diff > secondsBetweenLock) { // Timeout passed, lock
				ui.lockRequested();
				ui.LockerWorkerNotify(-1, secondsBetweenLock);
			} else {
				ui.LockerWorkerNotify(secondsBetweenLock - diff, secondsBetweenLock);
			}
		}
	}

	private long lastUpdate;

	public void update() {
		lastUpdate = System.currentTimeMillis(); // Do not divite it here, as it'S heavily used function
	}

	private void wait(int seconds) {
		synchronized (syncer) {
			try {
				syncer.wait(seconds * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private final Object syncer = new Object();

	private MainUI ui;

	public void stop() {
		thr.interrupt();
	}

	public LockWorker(MainUI ui) {
		this.ui = ui;
		update();
		thr.start();
	}
}

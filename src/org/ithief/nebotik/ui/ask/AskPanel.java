package org.ithief.nebotik.ui.ask;

import net.miginfocom.swing.MigLayout;

import java.awt.GridBagLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import org.ithief.nebotik.ask.AskClient;
import org.ithief.nebotik.database.SenderDatabase;
import org.ithief.nebotik.ui.Executor;
import org.ithief.nebotik.ui.MainUI;
import org.ithief.nebotik.ui.MyRunnable;
import org.ithief.nebotik.ui.main.CommonServicePanel;
import org.ithief.nebotik.ui.my.MyButton;
import org.ithief.nebotik.ui.my.MyLabel;
import org.ithief.nebotik.ui.my.MyPanel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AskPanel extends CommonServicePanel {

	private boolean runEnabled = false;
	protected int myErrors;
	private MyLabel lblUpd;
	private MyLabel lblErr;
	private MyLabel lblRun;
	private MyButton btnRun;
	private MyLabel lblCop;

	private static final String runStr = "RunEnabled";

	private void update(boolean en, MyLabel lbl, MyButton btn) {
		if (en) {
			btn.setText("Disable");
			lbl.setText("Enabled");
			lbl.setForeground(Color.green);
		} else {
			btn.setText("Enable");
			lbl.setText("Disabled");
			lbl.setForeground(Color.red);
		}
	}

	private AskClient ask;

	public void setAskEnabled(boolean en) {
		this.runEnabled = en;
		update(en, lblRun, btnRun);
		if (runEnabled) { // Enabled client
			if (ask != null) { // Already running
				return;
			} else {
				ask = new AskClient(this);
			}
		} else { // Disable client
			if (ask == null) { // Client not running
				return;
			} else {
				ask.stop();
				ask = null;
			}
		}
	}

	public void setLastUpdated(String lastUpdated) {
		lblUpd.setText(lastUpdated);
	}

	public AskPanel(final MainUI ui) {
		super(ui, "Ask.fm");
		super.setIcon("/org/ithief/nebotik/ui/ask/icon.png");
		this.db = new SenderDatabase(ui.getDB(), "AskSettings");
		this.runEnabled = db.contains(runStr);

		setName("Ask.fm");
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		MyPanel pnlInfo = new MyPanel();
		GridBagConstraints gbc_pnlInfo = new GridBagConstraints();
		gbc_pnlInfo.insets = new Insets(0, 0, 5, 0);
		gbc_pnlInfo.fill = GridBagConstraints.BOTH;
		gbc_pnlInfo.gridx = 0;
		gbc_pnlInfo.gridy = 0;
		add(pnlInfo, gbc_pnlInfo);
		pnlInfo.setLayout(new MigLayout("", "[][][grow][]", "[][][]"));

		MyLabel mlblLastUpdate = new MyLabel((String) null);
		mlblLastUpdate.setText("Last update:");
		pnlInfo.add(mlblLastUpdate, "cell 0 0,alignx right");

		lblUpd = new MyLabel((String) null);
		lblUpd.setText("");
		pnlInfo.add(lblUpd, "cell 1 0");

		MyButton mbtnUpdate = new MyButton((String) null);
		mbtnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				forceUpdate();
			}
		});
		mbtnUpdate.setText("Update");
		pnlInfo.add(mbtnUpdate, "cell 3 0,alignx center,growy");

		MyLabel mlblTotalErrors = new MyLabel((String) null);
		mlblTotalErrors.setText("Total errors:");
		pnlInfo.add(mlblTotalErrors, "cell 0 1,alignx right");

		lblErr = new MyLabel((String) null);
		lblErr.setText("");
		pnlInfo.add(lblErr, "cell 1 1");

		MyButton mbtnClear = new MyButton((String) null);
		mbtnClear.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				myErrors = 0;
				handleNewError(0);
			}
		});
		mbtnClear.setText("Clear");
		pnlInfo.add(mbtnClear, "cell 3 1,grow");

		MyLabel mlblCurrentOperation = new MyLabel((String) null);
		mlblCurrentOperation.setText("Current operation:");
		pnlInfo.add(mlblCurrentOperation, "cell 0 2,alignx right");

		lblCop = new MyLabel((String) null);
		lblCop.setText("");
		pnlInfo.add(lblCop, "cell 1 2 3 1");

		MyPanel pnlControl = new MyPanel();
		GridBagConstraints gbc_pnlControl = new GridBagConstraints();
		gbc_pnlControl.fill = GridBagConstraints.BOTH;
		gbc_pnlControl.gridx = 0;
		gbc_pnlControl.gridy = 2;
		add(pnlControl, gbc_pnlControl);
		pnlControl.setLayout(new MigLayout("", "[][][grow][]", "[]"));

		MyLabel mlblRun = new MyLabel((String) null);
		mlblRun.setText("Run:");
		pnlControl.add(mlblRun, "cell 0 0,alignx right");

		lblRun = new MyLabel((String) null);
		lblRun.setText("Enabled");
		pnlControl.add(lblRun, "cell 1 0,alignx left");

		btnRun = new MyButton((String) null);
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runEnabled = !runEnabled;
				setAskEnabled(runEnabled);
				save();
			}
		});
		btnRun.setText("Disable");
		pnlControl.add(btnRun, "cell 3 0,grow");
		load(ui);
	}

	private void load(MainUI ui) {
		setAskEnabled(runEnabled);
		if (runEnabled) {
			setAskEnabled(true);
		}
	}

	private final SenderDatabase db;

	private void saveStr(boolean b, String str) {
		if (b) {
			if (!db.contains(str)) {
				db.add(str);
			}
		} else {
			if (db.contains(str)) {
				db.remove(str);
			}
		}
	}

	private void save() {
		saveStr(runEnabled, runStr);
		db.save();
	}

	@Override
	protected void timestamp() {
		if (lblUpd != null) {
			lblUpd.setText(super.getTimestamp());
		}
	}

	private static final long serialVersionUID = -7190079109696132385L;

	@Override
	public void handleNewError(int totalCount) {
		Executor.execute(errorChanger, totalCount);
	}

	@Override
	public void forceUpdate() {
		ask.forceUpdate();
	}

	@Override
	public void startService() {
		Executor.execute(askServiceStarter, true);
	}

	@Override
	public void stopService() {
		Executor.execute(askServiceStarter, false);
	}

	@Override
	public void handleStatusChanged(String string) {
		Executor.execute(textChanger, string);
	}

	private MyRunnable errorChanger = new MyRunnable() {

		@Override
		public void run(Object obj) {
			int totalCount = (int) obj;
			myErrors += totalCount;
			lblErr.setText(myErrors + "");
		}

	};

	private MyRunnable textChanger = new MyRunnable() {

		@Override
		public void run(Object obj) {
			String str = (String) obj;
			lblCop.setText(str);
		}

	};

	private MyRunnable askServiceStarter = new MyRunnable() {

		@Override
		public void run(Object obj) {
			boolean b = (boolean) obj;
			setAskEnabled(b);
		}

	};
}

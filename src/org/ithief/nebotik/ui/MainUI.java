package org.ithief.nebotik.ui;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.ithief.nebotik.database.Database;
import org.ithief.nebotik.ui.ask.AskPanel;
import org.ithief.nebotik.ui.fb.FacebookPanel;
import org.ithief.nebotik.ui.main.LockPanel;
import org.ithief.nebotik.ui.main.LockStatusPanel;
import org.ithief.nebotik.ui.main.MainStatusDataPanel;
import org.ithief.nebotik.ui.main.MainStatusPanel;
import org.ithief.nebotik.ui.my.MyPanel;
import org.ithief.nebotik.ui.my.MyTabbedPane;
import org.ithief.nebotik.ui.skype.SkypePanel;

import java.awt.BorderLayout;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

public class MainUI extends JFrame {

	private MainStatusPanel statusPanel;

	private LockPanel lockPanel;
	private LockStatusPanel lockStatusPanel;
	private final MainStatusDataPanel status;

	private boolean added = true;
	private final LockWorker lw;

	private final Database db = new Database(this, "mango.db");

	public static final Color foregroundColor = Color.white;
	public static final Color backgroundColor = Color.black;

	private List<MyPanel> hiddenComponents = new ArrayList<>();

	private void removeStatusPanel() {
		if (added) {
			for (MyPanel cmp : hiddenComponents) {
				tabbedPane.remove(cmp);
			}
			added = false;
		}
	}

	private void addStatusPanel() {
		if (!added) {
			for (MyPanel cmp : hiddenComponents) {
				tabbedPane.addTab(cmp.getName(), null, cmp, null);
			}
			added = true;
		}
	}

	private Runnables runnables = new Runnables();

	public MainStatusDataPanel getStatusPanel() {
		return status;
	}

	private class Runnables {

		private MyRunnable unlocker = new MyRunnable() {

			@Override
			public void run(Object obj) {
				lockPanel.setLocked(false);
				lockStatusPanel.setLocked(false);
				addStatusPanel();
			}

		};
		private MyRunnable dbshower = new MyRunnable() {

			@Override
			public void run(Object obj) {
				boolean show = (boolean) obj;
				lockStatusPanel.setDBShowing(show);
			}

		};
		private MyRunnable lockerWorkerNotifier = new MyRunnable() {

			@Override
			public void run(Object obj) {
				Object[] o = (Object[]) obj;
				long diff = (long) o[0];
				int max = (int) o[1];
				lockStatusPanel.updateLocker((int) diff, max);
			}

		};
		private MyRunnable newerrer = new MyRunnable() {

			@Override
			public void run(Object obj) {
				boolean newErr = (boolean) obj;
				lockStatusPanel.setNewError(newErr);
			}

		};

		private MyRunnable locker = new MyRunnable() {

			@Override
			public void run(Object obj) {
				lockPanel.setLocked(true);
				lockStatusPanel.setLocked(true);
				removeStatusPanel();
			}

		};

	}

	private MyTabbedPane tabbedPane;

	private final MouseMotionAdapter mma = new MouseMotionAdapter() {
		@Override
		public void mouseMoved(MouseEvent e) {
			lw.update();
		}
	};

	public MainUI() {
		lw = new LockWorker(MainUI.this);
		status = new MainStatusDataPanel(this, "Status", false);
		status.setErrorAcceptListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MainUI.this.setError(false);
			}
			
		});
		try {
			SwingUtilities.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					UIManager.put("TabbedPane.contentOpaque", false);
					construct();
					handleFullScreen();
					distributeMMA(MainUI.this);
				}

			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}

	}

	private void distributeMMA(Component root) {
		root.addMouseMotionListener(mma);
		if (root instanceof Container) {
			for (Component cmp : ((Container) root).getComponents()) {
				distributeMMA(cmp);
			}
		}
	}

	private void handleFullScreen() {
		if (!new File("fs").exists()) {
			this.setUndecorated(true);
			setExtendedState(JFrame.MAXIMIZED_BOTH);
		}
		setVisible(true);
	}

	private void construct() {
		setTitle("Nebotik :)");
		getContentPane().setForeground(Color.WHITE);
		getContentPane().setBackground(Color.BLACK);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));

		lockStatusPanel = new LockStatusPanel(this);
		lockStatusPanel.setOpaque(false);
		getContentPane().add(lockStatusPanel, BorderLayout.NORTH);

		tabbedPane = new MyTabbedPane(MyTabbedPane.TOP);
		getContentPane().add(tabbedPane, BorderLayout.CENTER);

		lockPanel = new LockPanel(this);
		tabbedPane.addTab("Lock", null, lockPanel, null);

		statusPanel = new MainStatusPanel();
		statusPanel.addTab(status);

		hiddenComponents.add(statusPanel);
		addHiddenComponent(new FacebookPanel(this));
		addHiddenComponent(new AskPanel(this));
		addHiddenComponent(new SkypePanel(this));
		

		Dimension sz = new Dimension(480, 320);
		setBackground(backgroundColor);

		setSize(sz);
		setMinimumSize(sz);
		lockRequested();
	}

	private void addHiddenComponent(ServiceUI c) {
		hiddenComponents.add((MyPanel) c);
		statusPanel.addTab(c.getDataPanel());
	}

	private static final long serialVersionUID = -217514645114533831L;

	public static final boolean isOpaque = false;

	public void unlockRequested() {
		lw.update();
		Executor.execute(runnables.unlocker);
	}

	public void lockRequested() {
		Executor.execute(runnables.locker);
	}

	public Database getDB() {
		return db;
	}

	public void setDBShowing(boolean showing) {
		Executor.execute(runnables.dbshower, showing);
	}

	protected void LockerWorkerNotify(long diff, int max) {
		Executor.execute(runnables.lockerWorkerNotifier, new Object[] { diff, max });
	}

	public void setError() {
		setError(true);
	}

	public LockStatusPanel getLockStatusPanel() {
		return lockStatusPanel;
	}

	public void setGlobalError() {
		Executor.execute(runnables.newerrer, true);
	}

	public void setError(boolean hasError) {
		Executor.execute(runnables.newerrer, hasError);
	}
}

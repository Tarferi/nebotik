package org.ithief.nebotik.ui.skype;

import net.miginfocom.swing.MigLayout;

import java.awt.GridBagLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import org.ithief.nebotik.database.SenderDatabase;
import org.ithief.nebotik.objects.Message;
import org.ithief.nebotik.skype.SkypeClient;
import org.ithief.nebotik.skype.SkypeMessage;
import org.ithief.nebotik.ui.Executor;
import org.ithief.nebotik.ui.MainUI;
import org.ithief.nebotik.ui.MessageServiceUI;
import org.ithief.nebotik.ui.MyRunnable;
import org.ithief.nebotik.ui.main.CommonServicePanel;
import org.ithief.nebotik.ui.my.MyButton;
import org.ithief.nebotik.ui.my.MyLabel;
import org.ithief.nebotik.ui.my.MyPanel;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class SkypePanel extends CommonServicePanel implements MessageServiceUI {

	private boolean messagesEnabled = false;
	private boolean commandsEnabled = false;
	private boolean behaviorEnabled = false;
	private boolean runEnabled = false;
	private boolean acceptingRequests = false;

	protected int myErrors;
	private MyLabel lblUpd;
	private MyLabel lblErr;
	private MyLabel lblRun;
	private MyButton btnRun;
	private MyLabel lblAccept;
	private MyButton btnAccept;
	private SkypeClient skype;
	private MyLabel lblBhv;
	private MyButton btnBhv;
	private MyLabel lblMsg;
	private MyButton btnMsg;

	private static final String runStr = "RunEnabled";
	private static final String msgStr = "MessagesEnabled";
	private static final String bhvStr = "BehaviorEnabled";
	private static final String cmdStr = "CommandsEnabled";
	private static final String acceptStr = "AcceptEnabled";

	private void update(boolean en, MyLabel lbl, MyButton btn) {
		if (en) {
			btn.setText("Disable");
			lbl.setText("Enabled");
			lbl.setForeground(Color.green);
		} else {
			btn.setText("Enable");
			lbl.setText("Disabled");
			lbl.setForeground(Color.red);
		}
	}

	private void setAcceptEnabled(boolean en) {
		acceptingRequests = en;
		update(en, lblAccept, btnAccept);
		save();
	}

	public void setSkypeEnabled(boolean en) {
		this.runEnabled = en;
		update(en, lblRun, btnRun);

		if (runEnabled) { // Enabled client
			if (skype != null) { // Already running
				return;
			} else {
				skype = new SkypeClient(this);
			}
		} else { // Disable client
			if (skype == null) { // Client not running
				return;
			} else {
				skype.close();
				skype = null;
			}
		}
		save();
	}

	public void setLastUpdated(String lastUpdated) {
		lblUpd.setText(lastUpdated);
	}

	public SkypePanel(final MainUI ui) {
		super(ui, "Skype");
		super.setIcon("/org/ithief/nebotik/ui/skype/icon.png");
		this.db = new SenderDatabase(ui.getDB(), "SkypeSettings");
		this.runEnabled = db.contains(runStr);
		this.acceptingRequests = db.contains(acceptStr);
		this.commandsEnabled = db.contains(cmdStr);
		this.behaviorEnabled = db.contains(bhvStr);
		this.messagesEnabled = db.contains(msgStr);

		setName("Skype");
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		MyPanel pnlInfo = new MyPanel();
		GridBagConstraints gbc_pnlInfo = new GridBagConstraints();
		gbc_pnlInfo.insets = new Insets(0, 0, 5, 0);
		gbc_pnlInfo.fill = GridBagConstraints.BOTH;
		gbc_pnlInfo.gridx = 0;
		gbc_pnlInfo.gridy = 0;
		add(pnlInfo, gbc_pnlInfo);
		pnlInfo.setLayout(new MigLayout("", "[][][grow][]", "[][]"));

		MyLabel mlblLastUpdate = new MyLabel((String) null);
		mlblLastUpdate.setText("Last event:");
		pnlInfo.add(mlblLastUpdate, "cell 0 0,alignx right");

		lblUpd = new MyLabel((String) null);
		lblUpd.setText("");
		pnlInfo.add(lblUpd, "cell 1 0");

		MyLabel mlblTotalErrors = new MyLabel((String) null);
		mlblTotalErrors.setText("Total errors:");
		pnlInfo.add(mlblTotalErrors, "cell 0 1,alignx right");

		lblErr = new MyLabel((String) null);
		lblErr.setText("0");
		pnlInfo.add(lblErr, "cell 1 1");

		MyButton mbtnClear = new MyButton((String) null);
		mbtnClear.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				myErrors = 0;
				errorCountChanged(0);
			}
		});
		mbtnClear.setText("Clear");
		pnlInfo.add(mbtnClear, "cell 3 1,grow");

		MyPanel pnlControl = new MyPanel();
		GridBagConstraints gbc_pnlControl = new GridBagConstraints();
		gbc_pnlControl.fill = GridBagConstraints.BOTH;
		gbc_pnlControl.gridx = 0;
		gbc_pnlControl.gridy = 2;
		add(pnlControl, gbc_pnlControl);
		pnlControl.setLayout(new MigLayout("", "[][][grow][]", "[][][][][]"));

		MyLabel mlblRun = new MyLabel((String) null);
		mlblRun.setText("Run:");
		pnlControl.add(mlblRun, "cell 0 0,alignx right");

		lblRun = new MyLabel((String) null);
		lblRun.setText("Enabled");
		pnlControl.add(lblRun, "cell 1 0,alignx left");

		btnRun = new MyButton((String) null);
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runEnabled = !runEnabled;
				setSkypeEnabled(runEnabled);
				save();
			}
		});
		btnRun.setText("Disable");
		pnlControl.add(btnRun, "cell 3 0,grow");

		MyLabel mlblProcessBehavior = new MyLabel((String) null);
		mlblProcessBehavior.setText("Process behavior:");
		pnlControl.add(mlblProcessBehavior, "cell 0 1,alignx right");

		lblBhv = new MyLabel((String) null);
		lblBhv.setText("Enabled");
		pnlControl.add(lblBhv, "cell 1 1,alignx left");

		btnBhv = new MyButton((String) null);
		btnBhv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setBehaviorEnabled(!behaviorEnabled);
				save();
			}
		});
		btnBhv.setText("Disable");
		pnlControl.add(btnBhv, "cell 3 1,grow");

		MyLabel mlblProcessMessages = new MyLabel((String) null);
		mlblProcessMessages.setText("Process messages:");
		pnlControl.add(mlblProcessMessages, "cell 0 2,alignx right");

		lblMsg = new MyLabel((String) null);
		lblMsg.setText("Enabled");
		pnlControl.add(lblMsg, "cell 1 2,alignx left");

		btnMsg = new MyButton((String) null);
		btnMsg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setMessagesEnabled(!messagesEnabled);
				save();
			}
		});
		btnMsg.setText("Disable");
		pnlControl.add(btnMsg, "cell 3 2,grow");

		mlblProcessCommands = new MyLabel((String) null);
		mlblProcessCommands.setText("Process commands:");
		pnlControl.add(mlblProcessCommands, "cell 0 3,alignx right");

		lblCmd = new MyLabel((String) null);
		lblCmd.setText("Enabled");
		pnlControl.add(lblCmd, "cell 1 3,alignx center");

		btnCmd = new MyButton((String) null);
		btnCmd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCommandsEnabled(!commandsEnabled);
				save();
			}
		});
		btnCmd.setText("Disable");
		pnlControl.add(btnCmd, "cell 3 3,grow");

		MyLabel mlblAcceptRequests = new MyLabel((String) null);
		mlblAcceptRequests.setText("Accept requests:");
		pnlControl.add(mlblAcceptRequests, "cell 0 4,alignx right");

		lblAccept = new MyLabel((String) null);
		lblAccept.setText("Enabled");
		pnlControl.add(lblAccept, "cell 1 4,alignx left");

		btnAccept = new MyButton((String) null);
		btnAccept.setEnabled(false);
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setAcceptEnabled(!acceptingRequests);
			}
		});
		btnAccept.setText("Disable");
		pnlControl.add(btnAccept, "cell 3 4,grow");
		load(ui);
	}

	private void load(MainUI ui) {
		setBehaviorEnabled(behaviorEnabled);
		setCommandsEnabled(commandsEnabled);
		setMessagesEnabled(messagesEnabled);
		setAcceptEnabled(acceptingRequests);
		setSkypeEnabled(runEnabled);
	}

	private final SenderDatabase db;

	private void saveStr(boolean b, String str) {
		if (b) {
			if (!db.contains(str)) {
				db.add(str);
			}
		} else {
			if (db.contains(str)) {
				db.remove(str);
			}
		}
	}

	private void save() {
		saveStr(runEnabled, runStr);
		saveStr(acceptingRequests, acceptStr);
		saveStr(commandsEnabled, cmdStr);
		saveStr(messagesEnabled, msgStr);
		saveStr(behaviorEnabled, bhvStr);
		db.save();
	}

	public void errorCountChanged(int errors) {
		this.myErrors += errors;
		lblErr.setText(myErrors + "");
	}

	public void timestamp() {
		if (lblUpd != null) {
			lblUpd.setText(super.getTimestamp());
		}
	}

	private static final long serialVersionUID = -7190079109696132385L;

	@Override
	public void handleNewError(int totalCount) {
		Executor.execute(errorChanger, totalCount);
	}

	@Override
	public void forceUpdate() {
	}

	@Override
	public void startService() {
		Executor.execute(skypeServiceStarter, true);
	}

	@Override
	public void stopService() {
		Executor.execute(skypeServiceStarter, false);
	}

	@Override
	public void handleStatusChanged(String string) {
	}

	@Override
	public void handleMessage(String message) {
		super.handleMessage(message);
		super.setTextOnMainScreen("S", Color.yellow);
	}

	public boolean isAcceptingRequests() {
		return acceptingRequests;
	}

	private MyRunnable errorChanger = new MyRunnable() {

		@Override
		public void run(Object obj) {
			int totalCount = (int) obj;
			myErrors += totalCount;
			lblErr.setText(myErrors + "");
		}

	};

	private MyRunnable skypeServiceStarter = new MyRunnable() {

		@Override
		public void run(Object obj) {
			boolean b = (boolean) obj;
			setSkypeEnabled(b);
		}

	};
	private MyLabel mlblProcessCommands;
	private MyLabel lblCmd;
	private MyButton btnCmd;

	private void validateSettings() {
		if (!this.behaviorEnabled) { // If no behavior is enabled, turn off the rest as well 
			if (this.commandsEnabled) {
				this.setCommandsEnabled(false);
			}
			if (this.messagesEnabled) {
				this.setMessagesEnabled(false);
			}
		}
	}

	public void setBehaviorEnabled(boolean en) {
		this.behaviorEnabled = en;
		update(en, lblBhv, btnBhv);
		validateSettings();
	}

	public void setCommandsEnabled(boolean en) {
		this.commandsEnabled = en;
		update(en, lblCmd, btnCmd);
		validateSettings();
	}

	public void setMessagesEnabled(boolean en) {
		this.messagesEnabled = en;
		update(en, lblMsg, btnMsg);
		validateSettings();
	}

	@Override
	public boolean messagesAreEnabled() {
		return this.messagesEnabled;
	}

	@Override
	public boolean commandsAreEnabled() {
		return this.commandsEnabled;
	}

	private final List<SkypeMessage> unreadMessages = new ArrayList<>();

	@Override
	public void readMessage(Message msg) {
		if (unreadMessages.contains(msg)) {
			unreadMessages.remove(msg);
		}
	}

	@Override
	public void replyTo(Message msg, String response) {
		skype.replyTo(msg, response);
		readMessage(msg);
	}

	@Override
	public boolean isReadMessageEnabled() {
		return true;
	}

	@Override
	public List<Message> getMessages(boolean update) {
		List<Message> um = new ArrayList<>();
		um.addAll(unreadMessages);
		return um;
	}

	@Override
	public void processMessage(Message msg) {
		unreadMessages.add((SkypeMessage) msg);
	}

	@Override
	public boolean behaviorIsEnabled() {
		return this.behaviorEnabled;
	}

}

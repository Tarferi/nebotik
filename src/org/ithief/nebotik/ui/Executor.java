package org.ithief.nebotik.ui;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

public class Executor {
	public static void execute(MyRunnable r) {
		execute(r, null);
	}

	public static void execute(MyRunnable r, Object parameter) {
		if (SwingUtilities.isEventDispatchThread()) {
			r.run(parameter);
		} else {
			try {
				SwingUtilities.invokeAndWait(new Runnable() {

					@Override
					public void run() {
						r.run(parameter);
					}

				});
			} catch (InvocationTargetException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

package org.ithief.nebotik.ui;

import java.awt.Color;
import java.util.List;

import org.ithief.nebotik.database.Database;
import org.ithief.nebotik.ui.main.MainStatusDataPanel;
import org.ithief.nebotik.ui.my.MyLabel;

public interface ServiceUI {

	public void handleExceptions(List<Exception> exceptions);

	public void handleError(String error);

	public void handleMessage(String message);

	public void handleNewError(int totalCount);

	public void setStatus(ServiceStatus status);
	
	public ServiceStatus getStatus();
	
	public Database getDB();
	
	public void forceUpdate();
	
	public void startService();
	
	public void stopService();

	public void handleStatusChanged(String string);
	
	public String getName();
	
	public MyLabel getIcon();
	
	public MainStatusDataPanel getDataPanel();
	
	public void setTextOnMainScreen(String text, Color color);
	
	public void hideTextOnMainScreen();

}

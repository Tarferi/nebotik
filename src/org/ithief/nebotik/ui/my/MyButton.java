package org.ithief.nebotik.ui.my;

import javax.swing.JButton;

import org.ithief.nebotik.ui.MainUI;

public class MyButton extends JButton {

	private static final long serialVersionUID = 6529291068175810312L;

	public MyButton(String title) {
		super(title);
		setForeground(MainUI.foregroundColor);
		setBackground(MainUI.backgroundColor);
		
	}
}

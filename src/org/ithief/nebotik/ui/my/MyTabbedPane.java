package org.ithief.nebotik.ui.my;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

public class MyTabbedPane extends JTabbedPane {

	private static final long serialVersionUID = 7724315874896822098L;

	public MyTabbedPane(int top) {
		super(top);
		this.setUI(new MyTabbedPaneUI(this));
	}

	public MyTabbedPane() {
		super();
		this.setUI(new MyTabbedPaneUI(this));
	}

	private class MyTabbedPaneUI extends BasicTabbedPaneUI {

		private MyTabbedPane pnl;

		public MyTabbedPaneUI(MyTabbedPane pnl) {
			this.pnl = pnl;
		}

		@Override
		protected LayoutManager createLayoutManager() {
			return new MyTabbedPaneLayout();
		}

		@Override
		protected void paintTab(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect) {
			Color background = Color.black;
			Color border = Color.white;
			Color text = Color.white;
			if (pnl.getSelectedIndex() == tabIndex) { // Selected tab
				border = Color.cyan;
				text = Color.cyan;
			}

			Color savedColor = g.getColor();
			g.setColor(background);
			g.fillRect(rects[tabIndex].x, rects[tabIndex].y, rects[tabIndex].width, rects[tabIndex].height);
			g.setColor(border);
			g.drawRect(rects[tabIndex].x, rects[tabIndex].y, rects[tabIndex].width, rects[tabIndex].height);
			String title = pnl.getTitleAt(tabIndex);
			Rectangle2D textR = g.getFontMetrics().getStringBounds(title, g);
			int w1 = (int) textR.getWidth();
			int h1 = (int) textR.getHeight();
			int w = (rects[tabIndex].width - w1) / 2;
			int h = (rects[tabIndex].height - h1);
			g.setColor(text);
			g.drawString(title, rects[tabIndex].x + w, rects[tabIndex].y + rects[tabIndex].height - h);
			g.setColor(savedColor);
		}

		private class MyTabbedPaneLayout extends TabbedPaneLayout {
			@Override
			protected void padSelectedTab(int tabPlacement, int selectedIndex) {
			}
		}
	}

}

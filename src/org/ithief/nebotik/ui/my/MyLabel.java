package org.ithief.nebotik.ui.my;

import javax.swing.JLabel;

import org.ithief.nebotik.ui.MainUI;

public class MyLabel extends JLabel {

	private static final long serialVersionUID = 3906122551032452260L;

	public MyLabel(String title) {
		super(title);
		setForeground(MainUI.foregroundColor);
		setBackground(MainUI.backgroundColor);
	}
}
